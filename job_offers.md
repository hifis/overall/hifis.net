---
title: Careers
title_image: default
layout: default
excerpt:
    Job offers related to HIFIS
redirect_from: jobs
---

# Job Offers related to HIFIS
{:.text-success}

* [Leitung Sektion eScience-Zentrum (w_m_d)](https://www.gfz-potsdam.de/karriere/stellenangebote/details/10095)

---

# Further offers

* [Data Science Jobs at Helmholtz](https://www.helmholtz-hida.de/en/jobs/job-offers/)
* [Further vacancies within the Helmholtz Association](https://www.helmholtz.de/en/jobs_talent/job_vacancies/)
