---
title: Combine Services to Workflows by Service Orchestration.
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
layout: services/default
author: none
additional_css:
  - services/services-page-images.css
excerpt: >-
    Enhancing the composability of Helmholtz Cloud services to enable scientific workflow pipelines.
---

# Build the Workflow That Works for You and Fully Benefit from the Cloud Potential!

Usually, scientists have to do some manual steps to feed the results of a service into the next one - and then into the third, fourth and fifth. But now they can simplify their work. By composing the [Helmholtz Cloud](https://helmholtz.cloud/) services to pipelines, the services interact. Processing, analysing and interpreting: adapted to the use case, the orchestrated services process the scientific data as an automatic sequence.

HIFIS is committed to prioritise the seamless linking of complementary services, with a particular focus on scientific workflow pipelines.

<div class="image-block">
    <img
        class="help-image right"
        alt="Illustration of scientist doing research"
        src="{{ site.directory.images | relative_url }}illustrations/undraw_science_re_mnnr.svg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>Key aspects of service orchestration scientific workflow pipelines include:</p>
        <ul>
            <li><strong>Automation</strong>: Accelerating data processing and reducing errors.</li>
            <li><strong>Modularity</strong>: Allowing researchers to organise complex analyses into manageable tasks.</li>
            <li><strong>Reproducibility</strong>: Ensuring standardised, documented processes for result verification, including provenance information.</li>
            <li><strong>Scalability</strong>: Adapting seamlessly to varying data volumes and research scales, e.g. with the potential for automatic parallelisation.</li>
            <li><strong>Adaptability</strong>: Designed to evolve with changes, allowing researchers to update workflows as needed.</li>
            <li><strong>Collaboration</strong>: Supporting collaborative research efforts by enabling multiple researchers to contribute to the pipeline, fostering interdisciplinary cooperation and knowledge exchange.</li>
        </ul>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="Service Pipelines workflow"
        src="{{ site.directory.images | relative_url }}/services/service_pipelines_aas.svg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h2 id="minimal-service-orchestration">Set up of Minimal Service Orchestraion</h2>
        <p>By orchestrating these key components, as shown in the diagram, it is possible to build a minimal workflow pipeline, which is the basis for most of the more sophisticated scientific workflows that HIFIS would like to support. This Service Orchestration provides the following benefits:</p>
    </div>
</div>

<i class="fas fa-check-circle"></i> __Secure and controlled transfers__

All transfers and orchestration steps take place within the Helmholtz infrastructure, ensuring secure authentication and authorisation methods. 

Intermediate steps are finely controllable, allowing for privacy settings such as `private`, `group-shared`, or `public`, based on user preferences.

<i class="fas fa-check-circle"></i> __Flexible user access__

Different steps in the pipeline can be configured for accessibility by various user groups, providing granular control over workflow access.

<i class="fas fa-check-circle"></i> __Persistent workflows__

Workflows can persist for a defined timeframe, even when the original user(s) are offline. 

Continuous updates triggered by incoming raw data ensure real-time results, enhancing workflow adaptability.

<i class="fas fa-check-circle"></i> __Dynamic computational resources__

Easily scale compute power as needed at different points in time. 

Seamlessly exchange or replace storage and introduce specific processing steps using different cloud services, demonstrating the modularity of the setup.

In summary, even a minimal Service Orchestration provides a secure, flexible, and modular framework that ensures controlled access, persistence, and adaptability to changing computational and storage requirements.

<div class="image-block">
    <img
        class="help-image right"
        alt="Orchestrated services for the Helmholtz Imaging pilot implementation"
        src="{{ site.directory.images | relative_url }}/services/orchestration_components.png"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h2>Scientific Workflows using Helmholtz Cloud Services</h2>

        <p>To address the many technical challenges behind Service Orchestration, HIFIS is consulting with various research groups to setup initial building blocks of scientific workflows. Starting from specific use cases, we want to improve the composability of our services and make future workflows easier to set up. So far, we have focused on the minimal Service Orchestration components to enable the following use cases:</p>

        <p><a href="{% post_url 2023/2023-04-18-service-orchestration %}"><strong>Pilot Implementation in Helmholtz Imaging</strong></a>: Demonstrates the seamless interconnection of HIFIS Cloud services across five Helmholtz centres, showcasing modularity and utilising data from a research paper on intracellular organelles and insulin secretion.</p>
        
        <p><a href="{% post_url 2024/2024-01-24-uc-nest %}"><strong>NEST Desktop use case</strong></a>: A user-friendly, web-based GUI for the NEST Simulator, facilitating the creation and simulation of spiking neuronal network models without the need for programming skills. Powered by the HIFIS Cloud Service <a href="https://helmholtz.cloud/services/?serviceID=8da9d670-383c-4641-9896-fa25220cc0b5">"Rancher"</a> and Helmholtz ID.</p>        
    </div>
</div>

## Technical Basis to Connect Services
However, the composability of services for scientific Service Orchestration requires some key components:

- __Authentication and Authorisation Infrastructure (AAI)__: Enabling members of different institutions to access protected information distributed on various web servers.
- __Versioning and Revision Control__: Incorporating mechanisms to track changes for transparency and reproducibility.
- __Build Automation__: The process of automating tasks involved in compiling source code into executable programs or libraries, including testing code for faster development cycles and more reliable releases.
- __Data Storage and Ingestion__: Acquiring relevant data from various sources.
- __Compute Resources__: Managing the computational aspects, covering data processing, analysis, error handling, logging, data visualisation, and result output generation.

## Outlook for the Orchestration of Helmholtz Cloud Services

1. As we continue to develop use cases with several research groups, we will expand both our orchestration offering and the composability of existing cloud services. We believe that there is significant added value to be gained from service orchestration, and this is a point we have also heard from our users. [As already mentioned in 2023]({% link newsletter/2023-04/2023-04-HIFIS-Newsletter.md %}), we will therefore continue to focus on this task in 2024 and 2025 to improve our orchestration capabilities.

1. We will continue to develop the [minimal Service Orchestration](#minimal-service-orchestration) with the aim of creating a tutorial for researchers to initiate their own workflows from this base.

1. We will work to document the lessons learned and best practices on service orchestration in general, as our experience grows. 

1. [In collaboration with HMC]({% post_url 2024/2024-01-26-hifis-at-hmc-dialog %}), we plan to investigate generating standard and appropriate provenance information metadata for data that has been processed using one of our workflow pipelines. 

---

## <i class= "fas fa-address-book"></i> Queries, Comments and Ideas

If you have a use case or ideas about on our (scientific) Service Orchestration offer, please contact our  [__Service Orchestration team__](mailto:support@hifis.de).
