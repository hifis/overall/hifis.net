---
title: Helmholtz Backbone (VPN) <i class="fas fa-external-link-alt"></i>
title_image: pexels-brett-sayles-2881232.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "A trusted Helmholtz network to securely route scientific data transfer between centres."
redirect_to: https://hifis.net/doc/core-services/backbone-network/
---
