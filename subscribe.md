---
title: Subscribe to HIFIS lists
title_image: default
layout: default
excerpt:
    Subscribe to HIFIS lists.
---

<h3>Subscription links</h3>

See here for links to HIFIS newsletter, posts and specific announcement letters:

{% include subscription_lists.md %}

Spread the word and share the links. Thank you!
