---
title: "Der lange Weg zur Klarheit: Die rechtlich solide Basis steht"
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
date: 2024-04-30
authors:
  - spicker
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Cloud
  - Legal Aspects
lang: de
lang_ref: 2024-04-30-cloud-ruleset
excerpt: >
  Vier Jahre hat es gedauert, bis das Regelwerk der Helmholtz Cloud den verschiedenen Ansprüchen genügt hat. Nun haben sich 18 Helmholtz Zentren sowie die Geschäftsstelle geeinigt über die Regeln für Service Provider und Nutzergruppen in der Helmholtz Cloud.
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Preparation for Sending the Ruleset"
    src="{% link /assets/img/posts/2024-04-29-cloud-ruleset/20240429_114347-1.jpg %}"
    />
  </div>
</div>

# Der lange Weg zur Klarheit: Die rechtlich solide Basis steht

**Vier Jahre hat es gedauert, bis das Regelwerk der Helmholtz Cloud den verschiedenen Ansprüchen genügt hat. Nun haben sich 18 Helmholtz Zentren sowie die Geschäftsstelle geeinigt über die Regeln für Service Provider und Nutzergruppen in der Helmholtz Cloud.**

## Die Föderation gibt sich die Regeln selbst
Die Idee der Helmholtz Cloud ist eigentlich simpel: Jedes Helmholtz-Zentrum hat die Option, ausgewählte Services zu öffnen, damit andere Zentren sie nutzen
können. Alle Beteiligten der Helmholtz-Gemeinschaft waren sich schon 2019 einig, eine föderierte Cloud aufzubauen. Das schöpft Synergien aus, sowohl interdisziplinär in der Forschung wie auch in den IT Abteilungen. Sinnvoll in einer Zeit, in der Fachkräfte fehlen und Energiepreise steigen.

Die technische Umsetzung ist ein Glanzstück geworden: Die [Helmholtz AAI](https://hifis.net/aai) transferiert geschützt und datenschutzkonform die Logindaten des Mitarbeitenden in die wissenschaftliche Einrichtungen, die den Service hostet. Dieses Novum vereinfacht den Alltag so sehr, dass es inzwischen kaum noch wegzudenken ist. Es macht die über 20 Services der Helmholtz Cloud mühelos erreichbar. 

Doch wenn 19 wissenschaftliche Einrichtungen gemeinsam Services in einer Cloud zusammenstellen, reicht die technische Umsetzung nicht aus. Es kommen rechtliche Aspekte ins Spiel: So müssen der Umfang von Pflichten begrenzt, Ansprüche ausgeschlossen, Prozesse vereinbart werden. Das klingt zunächst nach der Aufzählung potenzieller Probleme. Doch dem HIFIS Team gelingt es geschickt, den Fokus auf die Vorteile der Helmholtz Cloud zu halten. 

## Und dann - die Steuerfrage
*"Und was ist eigentlich mit der Umsatzsteuer, gilt die nicht auch bei kostenfreien IT-Leistungen?"* Dass kostenfreie Dienste wie Youtube, Whatsapp oder Dropbox (um hier nur einige zu nennen) Umsatzsteuer bezahlen müssen, ist einleuchtend. Die Klärung, wie sich die Helmholtz Cloud von diesen kommerziellen Unternehmen unterscheidet, brauchte Zeit und Expertise. Der Unterschied liegt in der Aufgabe, die sich Helmholtz stellt. Es ist eine etablierten deutschen Forschungsorganisation, gefördert durch Bund und Länder. Der Zweck der Forschung aller Zentren ist die freie Forschung, es fehlt die Ausrichtung auf  kommerziellen Erfolg.  

HIFIS brachte die Experten aus den Bereichen Recht und Steuerrecht zusammen und zog externe Consultants hinzu. Die Diversität von Helmholtz mit zwei verschiedenen Ministerien als Geldgebern und unterschiedlichen Rechtsformen der Zentren erforderte eine sorgfältige Recherche über die Finanzierung der Helmholtz Cloud. Nur auf dieser detaillierten Basis konnte die Consultant-Kanzlei eine fundierte Aussage machen. Letztlich diktieren Umsatzsteuerrecht und Beihilferecht neue wichtige Klauseln in das Regelwerk mit dem Ergebnis: Die Services sind für Helmholtz untereinander kosten- und umsatzsteuerfrei! Externe Partner können einbezogen werden, jedoch nur unter der Leitung eines Helmholtz Partners. Die Auftragsforschung ist jedoch ausgeschlossen, genauso wie die private oder kommerzielle Nutzung: Hierfür dürfen Forschende die Helmholtz Cloud Services nicht verwenden. 

Die Gültigkeit des Regelwerks beendet die Aufbauphase. Für die Helmholtz Cloud beginnt jetzt eine neue Ära. 

## Fragen Sie uns
Wenn Sie gerne mehr wissen möchten, [schreiben Sie uns]({% link contact.md %}). Wir haben im Laufe der Zeit viel Erfahrung sammeln können und teilen diese 
gerne. Das Regelwerk ist jedoch ein vertrauliches Dokument, das nicht veröffentlicht werden wird. Wir bitten dafür um Verständnis.
