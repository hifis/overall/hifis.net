---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #005aa0}
</style>
<title>Discover the Helmholtz IT Services for Science!</title>
<style type="text/css">
body {height:100%!important;margin:0;padding:0;width:100%!important;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px;}
#m--background-table {margin:0;padding:0;width:100%!important;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px;}
table {mso-table-lspace:0pt;mso-table-rspace:0pt;}
table, table th, table td {border-collapse:collapse;}
a, img,a img {border:0;outline:none;text-decoration:none;}
img {-ms-interpolation-mode:bicubic;}
#outlook a {padding:0;}
.ReadMsgBody {width:100%;}
.ExternalClass {width:100%;display:block!important;}
.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass th,.ExternalClass td,.ExternalClass div {line-height: 100%;}
.yshortcuts,.yshortcuts a,.yshortcuts a:link,.yshortcuts a:visited,.yshortcuts a:hover,.yshortcuts a span {color:black;text-decoration:none!important;border-bottom:none!important;background:none!important;}
body,table,td,th,p,a,li,blockquote{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}
.preheader {display:none;display:none!important;mso-hide:all!important;mso-line-height-rule:exactly;visibility:hidden!important;line-height:0!important; font-size:0!important;opacity:0;color:transparent;height:0;width:0;max-height:0;max-width:0;overflow:hidden;}
@media only screen and (max-width: 580px) {
table[class*=m--hide], th[class*=m--hide], td[class*=m--hide], img[class*=m--hide], p[class*=m--hide], span[class*=m--hide] {display:none!important;}
#m--background-table img {
display:block;
height:auto!important;
line-height:100%;
min-height:1px;
width:100%!important;
}
#m--background-table img.nobreak,
#m--background-table img[class*=nobreak] {display:inline-block;}
.m--base,
.m--header,
.m--footer,
.m--section,
.m--content,
table[class*=m--base],
table[class*=m--header],
table[class*=m--footer],
table[class*=m--section],
table[class*=m--content] {
width:100%!important;
}
.m--base,
table[class*=m--base] {
width:95%!important;
}
.m--base-fullwidth,
.m--base-border,
table[class*=m--base-fullwidth],
table[class*=m--base-border] {
width:100%!important;
}
.m--base-fullwidth .m--section-container,
table[class*=m--base-fullwidth] td[class*=m--section-container] {
padding-left:5px;
padding-right:5px;
}
.m--base-fullwidth .m--section-container-fullwidth,
table[class*=m--base-fullwidth] td[class*=m--section-container-fullwidth] {
padding-left:0;
padding-right:0;
}
.m--base-fullwidth .m--section-container-fullwidth .m--content-container,
table[class*=m--base-fullwidth] td[class*=m--section-container-fullwidth] td[class*=m--content-container] {
padding-left:5px;
padding-right:5px;
}
.m--row-breakable .m--col,
table[class*=m--row-breakable] th[class*=m--col] {
display: block!important;
width:100%!important;
}
}
</style>
<!--[if gte mso 15]>
<style type="text/css">
.m--divider--inner {font-size:2px;line-height:2px;}
</style>
<![endif]-->
</head>
<body yahoo="fix" style="Margin:0;padding:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;background:#e6e6e6;">
<div class="preheader" style="display:none;display:none!important; mso-hide:all!important;visibility:hidden!important;mso-line-height-rule:exactly;line-height:0!important;font-size:0!important;opacity:0;color:transparent;height:0;width:0;max-height:0;max-width:0;overflow:hidden;">
Discover the Helmholtz IT Services for Science!
</div>
<table id="m--background-table" style="background:#e6e6e6;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--header" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="transparent" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<td class="fallback-topfooterfont" style="border-collapse:collapse;color:#999999;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:17px;" align="center">
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/subscribe/hifis-newsletter" target="_blank">Subscribe to HIFIS Newsletter</a> /
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/signoff/hifis-newsletter" target="_blank">Unsubscribe</a> /
<a style="text-decoration: none; color: #999999;" href="{% link newsletter/index.md %}" target="_blank">Archive</a>
<br><br>
</td>
<tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--base" style="border-collapse:separate;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#ffffff" align="center">
<table style="border-collapse:collapse;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--section-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--section" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#ffffff" align="center">
<table style="border-collapse:collapse;width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="bottom" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="bottom" align="center">
<table cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="center">
<a href="{% link index.md %}" alt="Link to HIFIS homepage">
<img style="max-width:380px;outline-style:none;text-decoration:none;border:none;font-size:12px;line-height:16px;margin:0;" src="hifis_logo_claim.png" alt="HIFIS Helmholtz Federated IT Services, Digital Services for Science =E2=80=94 Collaboration made easy." width="380" border="0">
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 0px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="top" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:0px;" valign="top" align="center">
<table class="m--button" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px; border:2px solid #ffffff;font-size:12px;padding:3px 9px;" bgcolor="#005aa0" align="center">
<a class="fallback-buttonfont" href="{% link newsletter/2022-12-xmas/2022-12-HIFIS-Newsletter.md %}" target="_blank" style="font-size:12px;line-height:18px;padding:3px 9px;font-weight:bold;font-family:'Open Sans', Arial, Helvetica, sans-serif;color:#ffffff;text-decoration: none;-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px;display:block;">
<span style="color: #ffffff;"><!--[if mso]>&nbsp;<![endif]-->December 2022<!--[if mso]>&nbsp;<![endif]--></span>
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:10px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">

<div><br>

<img src="andrea-music-2RFhuLEL8NI-unsplash.jpg" alt="blue baubles" border="0" align="center">

<div markdown="1">
{% include_relative editorial.md %}
</div>




</div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<br><table style="width:100%;font-size:0px;line-height:0px;border:none;border-top-width:2px;border-top-style:solid;border-top-color:#005aa0;border-collapse:separate;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="m--divider--inner" style="border-collapse:collapse;font-size:0px;line-height:0px;" valign="top" align="left"><div style="display:none;">&nbsp;</div></td>
</tr>
</tbody></table><br>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>




<tr style="line-height:0!important;font-size:0!important;height:0;">
<td style="border-collapse:collapse;line-height:0!important;font-size:0!important;height:0;" height="0"><span style="font-size:0;line-height:0;" id="jobs"></span></td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table class="m--button" style="border-collapse:separate;" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px; border:2px solid #ffffff;font-size:12px;padding:3px 9px;" bgcolor="#005aa0" align="center">
<a class="fallback-buttonfont" href="https://www.helmholtz-hida.de/en/jobs/job-offers/" target="_blank" style="font-size:12px;line-height:18px;padding:3px 9px;font-weight:bold;font-family:'Open Sans', Arial, Helvetica, sans-serif;color:#ffffff;text-decoration: none;-moz-border-radius:20px;-webkit-border-radius:20px;border-radius:20px;display:block;">
<span style="color: #ffffff;"><!--[if mso]>&nbsp;<![endif]-->Data Science Jobs at Helmholtz<!--[if mso]>&nbsp;<![endif]--></span>
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">
<div style="text-align:center"><span style="color:#999999">You can find our compulsory information<br>
and data privacy policy</span><span style="color:#005aa0"> </span><a style="text-decoration: none; " href="https://www.desy.de/imprint/index_eng.html"><span style="color:#005aa0">here</span></a><span style="color:#005aa0">.</span></div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="#ffffff" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-innerfont" style="border-collapse:collapse;color:#000000;font-family:'Open Sans', Arial, Helvetica, sans-serif;font-size:14px;line-height:20px;" align="left">
<div style="text-align:center"><span style="color:#999999">Imprint<br>
Editorial Responsibility:<br>
Lisa Klaffki, HIFIS Communication Officer<br>
support@hifis.net</span><br>
<span style="color:#999999">HIFIS Coordination: Uwe Jandt<br>
uwe.jandt@desy.de<br>
Deutsches Elektronen-Synchrotron DESY<br>
Notkestraße 85<br>
D-22607 Hamburg</span></div>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td style="border-collapse:collapse;" valign="top" bgcolor="#e6e6e6" align="center">
<table class="m--footer" style="border-collapse:collapse;width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--content-container" style="border-collapse:collapse;" valign="top" align="center">
<table class="m--content" style="width:580px;" width="580" cellspacing="0" cellpadding="0" border="0" align="center">
<tbody><tr>
<td class="m--container" style="border-collapse:collapse;padding:0px 10px 0px;" valign="top" bgcolor="transparent" align="center">
<table class="m--row" style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<th class="m--col" style="border-collapse:collapse;padding:0;font-size:1px;line-height:1px;font-weight:normal;width:100%;" width="100%" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td style="border-collapse:collapse;font-size:1px;line-height:1px;padding:10px;" valign="middle" align="center">
<table style="width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td class="fallback-topfooterfont" style="border-collapse:collapse;color:#999999;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:17px;" align="center">
<br>
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/subscribe/hifis-newsletter" target="_blank">Subscribe to HIFIS Newsletter</a> /
<a style="text-decoration: none; color: #999999;" href="https://lists.desy.de/sympa/signoff/hifis-newsletter" target="_blank">Unsubscribe</a> /
<a style="text-decoration: none; color: #999999;" href="{% link newsletter/index.md %}" target="_blank">Archive</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</th>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

</body></html>
