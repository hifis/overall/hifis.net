**Helmholtz Imaging Conference 2024: Registration & Call for Abstracts**

Join the 4th Helmholtz Imaging Conference on 14-15 May in Heidelberg - open to all scientists, tailored for imaging researchers. 

Experience keynotes, a poster session, match-making opportunities for the next HI Project call & more! Showcase your work, network & stay updated on imaging trends. NEW: “BYOIC - Bring Your Own Imaging Challenge” session. 

[Register, share your challenge & submit your abstract today!](https://events.hifis.net/event/1167/)