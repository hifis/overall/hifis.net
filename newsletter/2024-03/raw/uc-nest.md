**An Infrastructure as a Service use case with the NEST Desktop**

NEST is used in computational neuroscience to model and study the behaviour of large networks of neurons. It is co-developed by our colleagues at Jülich and is listed as one of the Software spotlights in the [Research Software Directory](https://helmholtz.software/software/nest).
We mentioned it in our last issue of the HIFIS newsletter, and now you can take a closer look at __how NEST uses the Helmholtz Cloud's Rancher service__ to provide an additional online service for using NEST worldwide and without installation. 

[Read more...]({% post_url 2024/2024-01-24-uc-nest %})