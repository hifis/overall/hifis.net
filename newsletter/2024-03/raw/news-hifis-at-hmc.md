**HIFIS at the HMC dialogue**

In January, we had a good opportunity to present HIFIS and to discuss ideas for more synergies with HMC. __The orchestration of distributed services__ was of particular interest to our HMC colleagues, as there is potential for HIFIS to e.g. improve the provenance information of processed datasets. Measuring and publishing the __energy consumption of cloud services__ was also a topic that we will follow up.

[Read more...]({% post_url 2024/2024-01-26-hifis-at-hmc-dialog %})