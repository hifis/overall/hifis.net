Dear HIFIS Community and Friends,

You are probably familiar with many of our highly demanded collaborative [Helmholtz Cloud Services](https://helmholtz.cloud/services/)?
If not, try them out and advise your colleagues to do the same!
During the last year however, we managed to onboard several services with somewhat more specific [scientific purpose](https://helmholtz.cloud/services/?filterKeywordIDs=5a8e6b9c-c2d9-4607-88cc-413d2f97289d), such as, for example, the Resource Entitlement Management System (REMS).
[Read here](#hifisnews) and in the subsequent newsletters to find out more about them, as any of them might be the perfectly fitting tool for your scientific workflow!

On top, our team has made significant improvements to our cloud services, such as the Research Software Directory (RSD), which includes some nice new features. 
Also take a look at our achievements to orchestrate Helmholtz Cloud services like Rancher Kubernetes for the sake of realising use cases together with scientific user groups. 

We are happy to introduce our software engineering education program for 2024:
This program is designed to foster a deeper understanding of good software engineering principles and practices for participants from all Helmholtz centers.

You might have heard of the security incident at HZB last year. We provide a sneak peek into some of the details and how HIFIS assisted HZB in the aftermath. 

Also stay informed on the [latest happenings from other platforms](#incubator)!

Last but definitely not least, mark your calendars for the upcoming [HIFIS annual meeting in September at DLR, Cologne]({% post_url 2024/2024-02-26-std-all-hands %}), an event open to all interested individuals from Helmholtz and partners. We look forward to seeing you there!

Enjoy reading and using our HIFIS services!
Make sure you spread the word and [subscribe to our newsletter](https://hifis.net/subscribe).

The HIFIS Team
