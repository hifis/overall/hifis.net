**Firsthand insight into the HZB cyberattack**

Nine months after HZB was victim of a cyberattack, Andreas tells us the story of what happened and how HIFIS could help right from the beginning.

[Read more...]({% post_url 2024/2024-03-21-hzb-cyber %})
