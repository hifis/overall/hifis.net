**Want to improve your scientific software skills in 2024?**

Our research software engineering tutors offer a wide range of training courses tailored for scientists, and the list for 2024 is now live. These are online courses, limited to just 25 participants for personal attention, run over 2-3 days and cover essential topics such as data processing, git, Python, and more. 

[Read more...]({% post_url 2024/2024-03-11-courses-2024 %})
