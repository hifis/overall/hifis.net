**Five new services at the Helmholtz Cloud**

We constantly add new services to the [Helmholtz Cloud](https://helmholtz.cloud/).
During the last months, we broadened our range of scientific services, from the collaborative LaTeX editor [Collabtex](https://helmholtz.cloud/services/?serviceID=59a9d116-a897-4eac-a8f5-b810ab3838fe) to [Helmholtz RSD](https://helmholtz.cloud/services/?serviceID=6f188651-a257-43fa-8878-0b7fb18d54b2), a software catalogue designated to Research Software that is being developed within the Helmholtz Association.
With these additions, we reached **thirty services**, provided by **nine Helmholtz centres**!

[Read more...]({% post_url 2023/2023-03-27-new-services-in-helmholtz-cloud-service-portfolio %})