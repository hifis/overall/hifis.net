**Orchestrated Cloud Services**

Interconnecting cloud services — sometimes dubbed Service Orchestration — allows to automate complex workflows, a precondition to ultimately set up reliable, scalable, reproducible and adaptable data processing pipelines for science.
A typical example is to automatically and continuously fetch, move, process, publish and store scientific data, possibly in multiple iterations and for different audiences.
We demonstrate our first pilot orchestration of an imaging use case in collaboration with Helmholtz Imaging.

[Read more...]({% post_url 2023/2023-04-18-service-orchestration %})