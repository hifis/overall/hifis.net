**Helmholtz Research Software Directory**

After six months of testing, HIFIS finally opened the [**Helmholtz Research Software Directory**](https://helmholtz.software/) (RSD) for all Helmholtz members.
It's a platform to promote your Research Software: After the login via [Helmholtz AAI](https://hifis.net/aai), you can add a description of your research software or project.

[Read more...](https://hifis.net/news/2023/02/17/rsd-open.html)