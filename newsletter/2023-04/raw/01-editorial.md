Dear HIFIS Community and Friends,

during the last months we have been busy to expand our [Helmholtz Cloud](https://helmholtz.cloud/) portfolio, both qualitatively and quantitatively:
An increasing number of dedicated scientific services have recently been onboarded,
such as AWI's Web Ocean Data Viewer, GFZ's new Research Software Directory or HZB's SciFlow.
Even more are expected to be added in the near future.
Overall, we now provide thirty services from nine Helmholtz centres towards the whole Helmholtz Association and collaboration partners.

On top, we strive to design so-called orchestrated service pipelines in close collaboration with scientific user groups.
This major further development is showcased with a use case HIFIS set up together with [Helmholtz Imaging](https://helmholtz-imaging.de/) and is presented in this issue.

A lot more is to be expected from HIFIS:
Have a look at the [roadmap]({% link timeline/index.md %}) for 2023!

Enjoy reading and using HIFIS Services!

The HIFIS Team
