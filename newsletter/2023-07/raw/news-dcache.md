**Exa-scale storage: Check dCache InfiniteSpace!**

dCache InfiniteSpace as the 32<sup>th</sup> Helmholtz Cloud service expands the service portfolio: Large scale storage is now available as a service to all user groups of Helmholtz and their partners. 
The service allows to store, process and publish research data with practically no storage limits.
It is a perfect match to combine with data processing pipelines (service orchestration) that the HIFIS team is currently setting up in collaboration with research groups.

[Read more...]({% post_url 2023/2023-07-11-dcache-in-helmholtz-cloud %})
