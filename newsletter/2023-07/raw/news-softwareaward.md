**Apply for the first Helmholtz Incubator Software Award**

The Helmholtz Incubator Software Award aims to promote the development of professional and high-quality research software and to recognize the commitment to software as the basis of modern Data Science. 

To apply, you have to register your contribution in the [Helmholtz Software Directory](https://helmholtz.software/)!

[Read more...]({% post_url 2023/2023-07-03-software-award %})
