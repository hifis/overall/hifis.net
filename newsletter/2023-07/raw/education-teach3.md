**Teach 3: Call for Abstracts and Registration are open**

The conference TEACH, the Teaching Across Communities at Helmholtz, will take place on 07./08.12.2023 for the third time. The call for abstracts is open till 13.09.2023.

[Read more...]({% post_url 2023/2023-07-03-teach3-call %})
