---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #005aa0}
</style>
<title>HIFIS News July 2023</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#incubator">News from Helmholtz Incubator</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#005aa0"><strong>News from HIFIS</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 20%;width: 108px;" src="{% link assets/img/posts/2022-06-29-use-case-dCache/dCache-logo.svg %}" alt="Raven as logo for dCache" border="0" align="right" />
<div markdown="1">
{% include_relative raw/news-dcache.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="Screenshot_Cloud.png" alt="Screenshot Cloud landing page" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-portal-relaunch.md %}
</div>


{% include_relative html_bits/section_separator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="HiDA_Visual_TEACH_16-9_230626.jpeg" alt="banner with text TEACH 3" border="0" align="right" />

<div markdown="1">
{% include_relative raw/education-teach3.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="Ausbildung.png" alt="banner with text Code for Thought" border="0" align="left" />
<div markdown="1">
{% include_relative raw/education-podcast.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="incubator"><span style="color:#005aa0"><span style="font-size:16px"><strong>News from the Helmholtz Incubator</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 20%;width: 108px;" src="incubator_logo.png" alt="Logo of Helmholtz Incubator" border="0" align="right" />

<div markdown="1">
{% include_relative raw/news-incubator-workshop.md %}
</div>



{% include_relative html_bits/section_subseparator.htm %}
<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="giorgio-trovato-_XTY6lD8jgM-unsplash.jpg" alt="Screenshot Cloud landing page" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-softwareaward.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="summer-academy-2023.png" alt="banner with text summer academy" border="0" align="right" />
<div markdown="1">
{% include_relative raw/news-incubator-summerschool.md %}
</div>


{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
