---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #00285a}
</style>
<title>HIFIS News February 2025</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#platforms">News from the Helmholtz Platforms</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#00285a"><strong>News from HIFIS</strong></span></span><br>

<a href="{% post_url 2025/2025-02-11-quality-indicators %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="undraw_awards_faq6_162px.png" alt="awards undraw illustration" border="0" align="right">
</a>
<div markdown="1">
{% include_relative raw/software-quality.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="{% post_url 2025/2025-01-08-HZB-LaTeX %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="undraw_share_link_re_54rx_162px.png" alt="share link undraw illustration" border="0" align="left">
</a>
<div markdown="1">
{% include_relative raw/federation.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="{% post_url 2024/2024-11-25-datahub-services %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="earth_data.jpg" alt="view of earth data webpage" border="0" align="right">
</a>
<div markdown="1">
{% include_relative raw/cloud-earth.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="{% link courses.md %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="undraw_building_blocks_re_5ahy_162px.png" alt="undraw illustration of building blocks" border="0" align="left">
</a>
<div markdown="1">
{% include_relative raw/software.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="{% post_url 2024/2024-11-26-think-tank-service-onboarding %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="juelich.jpeg" alt="Picture of Jülich computer center" border="0" align="right">
</a>
<div markdown="1">
{% include_relative raw/cloud-onboarding.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="{% post_url 2025/2025-01-09-Helmholtz-ID-simplifies-secure-login %}">
    <img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="Helmholtz-ID-logo_blue_162px.png" alt="Helmholtz ID logo" border="0" align="left">
</a>
<div markdown="1">
{% include_relative raw/backbone.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="platforms"><span style="color:#00285a"><span style="font-size:16px"><strong>News from the Helmholtz Platforms</strong></span></span></div>

<a href="https://bit.ly/HIConf25">
    <img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="HI Conference_Call4Abstracts_1200x627px_Thumbnail.jpg" alt="Helmholtz Imaging Conference 2025" border="0" align="left">
</a>

<div markdown="1">
{% include_relative raw/hi-conf.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="https://www.helmholtz-hida.de/en/events/hida-lecture-large-scale-brain-decoding/">
    <img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="HIDA-Lecture-Series-on-AI.png" alt="HIDA Lecture Series on AI" border="0" align="right">
</a>

<div markdown="1">
{% include_relative raw/hida-lecture.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<a href="http://www.helmholtz-metadaten.de/HMC_CON2025">
    <img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="HMC-Conference-2025_Web-CFA.jpg" alt="HMC Conference" border="0" align="left">
</a>

<div markdown="1">
{% include_relative raw/hmc-conf.md %}
</div>



{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
