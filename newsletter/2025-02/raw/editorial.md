Dear HIFIS Community and Friends,

For several years, the main focus under the umbrella of the HIFIS platform has been on activities to develop and offer scientific services, preferably in the cloud. This work is now producing visible results worldwide. The Helmholtz Research Field Earth and Environment has consistently followed this path with the Digital Earth project and there are now outstanding digital offerings that have also been integrated into the Helmholtz Cloud Platform, as you'll see in this issue.  

The success is also reflected on the [helmholtz.software](https://helmholtz.software/) platform, as the approximately 400 software solutions offered there are now referenced around 50,000 times in journals, conferences, books or PhD theses, many of them over 1,000 times alone.  

In the coming months, the 5 Helmholtz platforms will work together to give this fantastic development additional momentum, stay tuned!       

The HIFIS Team
