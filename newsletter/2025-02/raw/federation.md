**Trusting the Federated Services**

Imagine a future where instead of 18 duplicated instances of the same service, we have 3 or 4 across all Helmholtz centers? That's our vision for HIFIS and it is becoming a reality, with HZB leading the way. 

[Read more...]({% post_url 2025/2025-01-08-HZB-LaTeX %})