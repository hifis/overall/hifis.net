**HIDA Lecture: Large-Scale Brain Decoding**

Join Blake Richards' lecture on innovative techniques for training neural decoding models across diverse datasets on March 10. By using novel tokenization methods, these transformer-based models can integrate data from varied subjects, stimuli, brain regions, and species.  
Don’t miss out—[**register now**](https://www.helmholtz-hida.de/en/events/hida-lecture-large-scale-brain-decoding/) to explore the future of neural decoding in neuroscience.