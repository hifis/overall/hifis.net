**Refinement of Service Onboarding**

Onboarding new services is a challenge for service providers, but it has become routine for the HIFIS team. The process [described in detail here](https://www.hifis.net/doc/process-framework/Chapter-Overview/) involves numerous tasks and requirements and often, the technical integration has to be customized to fit the service.

To optimize this process, the cloud teams critically and constructively discussed ways to improve the entire workflow. Numerous ideas were collected at the meeting in Jülich in September 2024.

[Read more...]({% post_url 2024/2024-11-26-think-tank-service-onboarding %})
