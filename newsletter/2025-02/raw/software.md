**New Education Portfolio is Online!**

Check out the [2025 HIFIS Software Education Portfolio]({% link courses.md %})! Our joint course portfolio with HIDA has now expanded to include Helmholtz Imaging, Helmholtz AI and HMC. The portfolio covers a wide spectrum of Data Science and Software Engineering topics, and facilitates navigation to the right courses for your learning goals.