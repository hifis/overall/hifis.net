**New Recognition for Data and Research Software!**

Starting now, data publications and research software will be recognized in Helmholtz alongside peer-reviewed papers as research outputs. And HIFIS can help you make your software meet the criteria.

[Read more...]({% post_url 2025/2025-02-11-quality-indicators %})