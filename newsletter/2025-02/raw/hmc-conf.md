**HMC Conference 2025: Call for Abstracts is Open!**

We're thrilled to announce that abstract submission is officially open for the 4th HMC Conference! Join us in Cologne from May 12-14, 2025, for an exciting in-person event, where you can present, share and discuss your research to a diverse audience of metadata experts.  
Don't miss your chance to be part of this dynamic event. [**Submit your abstract by March 16**](http://www.helmholtz-metadaten.de/HMC_CON2025) and contribute to shaping the future of metadata!