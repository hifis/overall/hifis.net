**New Scientific Services Joined Helmholtz Cloud**

A fruitful cooperation has been established between the DataHub of the research area Earth and Environment and HIFIS. DataHub services utilize HIFIS services, including  [Helmholtz ID](https://hifis.net/aai/) for authentication and authorization, the [Helmholtz Research Software Directory (RSD)](https://helmholtz.software/) and the Helmholtz Cloud for access to data infrastructure services. This partnership supports the professionalization of scientific services and enables open access.

Currently, eight services from DataHub are offered on the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/?filterSearchInput=datahub&sortByAttribute=title) and thus also in the “Tools & Services” section of the [Earth Data Portal](https://earth-data.de/tools?qf=genericType/service&offset=0&q=DataHub).

[Read more...]({% post_url 2024/2024-11-25-datahub-services %})