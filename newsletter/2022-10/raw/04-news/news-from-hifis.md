HIFIS, the Helmholtz-wide platform for federated IT services, underwent a thorough review during August and September 2022, with the major event taking place at the coordinating centre DESY from Sept 29-30. The evaluation committee represents multiple national and international institutions and collaborations.

During two day’s sessions, the progress of HIFIS, as well as the plans for potential future development, have been presented and discussed in detail.

According to the preliminary response by the reviewers, the achieved progress of all three HIFIS clusters has been rated excellent. Multiple of the future plans of further strengthening the HIFIS portfolio have been strongly supported. The enthusiasm of the HIFIS team, spanning practical and efficient cooperation throughout Helmholtz, has been especially acknowledged.

[Read more...](https://hifis.net/news/2022/10/06/hifis-evaluation)
