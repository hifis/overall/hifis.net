**Helmholtz Career Day for Data Science and IT 2022**

The Career Day by HIDA is a great opportunity to meet data scientists and IT experts.
Join the conference program to learn about career paths,
and explore applied data science job opportunities.
[Register here](https://hopin.com/events/helmholtz-data-science-career-day-2022/registration) to
join the Helmholtz Career Day for Data Science and IT on November 15.

**2022 Helmholtz AI Project Call**

Being the core of its mission, Helmholtz AI is fostering cross-field creativity by stimulating collaborative
research projects in applied machine learning (ML) and artificial intelligence (AI).
Therefore, the annual Helmholtz AI project call,
funded by the Helmholtz Association's Initiative and Networking Fund (INF), aims to support this goal.
Get [more information here](https://www.helmholtz.de/assets/helmholtz_gemeinschaft/user_upload/Ausschreibungen/01_Helmholtz_AI_2022_projects_call.pdf) and
join the information event on November 10.
