The goal of **DAPHNE4NFDI** is to create a comprehensive infrastructure to process research data from large scale photon and neutron infrastructures. A project of this scale needs a reliable infrastructure to ensure communication, data exchange and publishing of the results. Therefore, DAPHNE4NFDI became the second major HIFIS user among the NFDI consortia.

[Read more about this use case...](https://hifis.net/use-case/2022/09/14/DAPHNE4NFDI)
