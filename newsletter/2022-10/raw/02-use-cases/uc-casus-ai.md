**AI Research in Health Topics at CASUS**:
The use of pseudonymised and anonymised patient data in a secure, federated way to better understand, diagnose and treat cancer is the big next step in enhancing the quality of decision making in clinics beyond what current guidelines can offer.

[Read more about this use case...](https://hifis.net/use-case/2022/08/10/use-case-health)
