[Do you remember me from the last newsletter?]({% link newsletter/2022-07/sam_july.md %})
I'm **Sam Scientist**.
When we last met, I was just starting my new research project.
I collaborate with some colleagues:
One is based at another Helmholtz Centre, and we teamed up with two researchers outside the Helmholtz universe.

I want to make the life of my colleagues easier by providing them with some automated routines for their image processing.
But the code is in bad shape...

[See how HIFIS Consulting helped me out to get on right track with great results]({% link newsletter/2022-10/sam_october.md %}).
