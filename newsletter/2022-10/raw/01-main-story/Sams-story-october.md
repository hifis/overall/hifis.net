---
title: Meet Sam the Scientist! Chapter 2
title_image: blue_curtains.jpg
layout: services/default
excerpt: >-
    No text.
---
# Sam's Analysis Project

[Do you remember me from the last newsletter?][last-met]
I'm Sam Scientist.
When we last met, I was just starting my new research project.
I collaborate with some colleagues: one is based at another Helmholtz centre, and we teamed up with two researchers outside the Helmholtz universe.

## Slowing down – Data Analysis by Hand

In the first weeks, we were pretty excited, like in the first week of a new semester at university.
We set up new projects at the [Helmholtz Codebase][codebase], renamed them once or twice and fleshed out the timetables from the proposal.
Now, some routine has set in. In the meantime, my colleagues need to study some video material, which is quite tiresome.
They take each frame and analyse it with [ImageJ2][ImageJ2].

Some images contain all the exciting stuff, of which they take notes.
However, many frames are in between stills and must be filtered out.
Your eyes and brain get exhausted and bored over time.
We discussed this approach in our weekly meeting: A quick estimate showed that this is using up too many resources overall.
We need to find a way to do this faster to concentrate on the scientific outcomes.

## Speeding up with a Script – but it's old and faulty

From my master's thesis, I have a program that could handle this task.
Maybe I should revisit it and get it running.
I could help out my co-workers.
It's old code, and I'm sure I never created enough documentation.
I should have made it more understandable to my future Me.
Yep. I just checked.
It's awful, to be honest.
But as I complained about my dilemma over a coffee,
my supervisor pointed out that HIFIS offers not only [cloud services][services] but also [consulting][consulting] for research software projects,
which is precisely what I need.
I hadn't thought about contacting them because it sounds so "big and serious",
and my program feels relatively tiny and student-ish.

**Who are you gonna call? HIFIS Consulting!**

HIFIS Consulting offers free-of-charge consulting to research groups within Helmholtz.
This happens through a defined process.
For the first contact, they provide a short questionnaire, which asks for a quick introduction to the project and problem at hand, the Helmholtz affiliation and the accessibility of the code.
After this introduction, the form gets into details, e.g. project status, field of research, and how many people will use and/or contribute to the project.
This ensures the request is directed towards a consultant with knowledge and expertise in the required areas.
After reassurance from my supervisor that my problem was within the scope of consulting, I filled out the request form and was amazed that it was done so quickly.
I click "Submit ". Now I wait. A consultant will get in touch with me.

The process is laid down in detail in the [HIFIS Consulting Handbook][handbook].
The website gives an overview of what will happen: Together with the consultant, I identify a plan of action to solve my issues.
They offer ongoing support but might advise on tools or give a recommendation for special software.
HIFIS also conducts [training sessions][training], which I already learned when I sent my team to the GitLab course.
In the numerous consultations, the consultants gather a lot of experience and share this information.
A look at the [awesome-rse][awesome-rse] list is therefore always worthwhile.

## Sneak View into the Future

Some weeks later: My code runs, and my colleagues are using it with good results. Cool!
I might even showcase it in the [Helmholtz Software Spotlights][spotlights] as I have cleaned it up.
But for now, we're still doing our research, but better: more efficient, more sustainable.
We can concentrate on the detailed analysis of more complex and essential frames.

<!-- LINKS -->
[last-met]: https://hifis.net/newsletter/2022-07/2022-07-HIFIS-Newsletter.html
[codebase]: https://codebase.helmholtz.cloud/
[ImageJ2]: https://imagej.net/software/imagej2/
[services]: https://helmholtz.cloud/services/
[consulting]: https://www.hifis.net/services/software/consulting.html
[handbook]: https://www.hifis.net/consulting-handbook/
[training]: https://www.helmholtz-hida.de/course-catalog/en/
[spotlights]: https://helmholtz.software/
[awesome-rse]: https://github.com/hifis-net/awesome-rse
