Dear HIFIS Community and Friends,

The past weeks have been exciting, as the HIFIS platform was evaluated.
In addition to presenting the results, figures and user feedback, we developed plans and visions for further development of HIFIS.
In the end, HIFIS was certified for its excellent development in all areas and a clear recommendation was made for the further operation and expansion of the platform.

We are offering new services to our rapidly growing number of users, and through close cooperation, new science-related services are emerging!
In this newsletter, we can again report on [exciting new community use cases]({% link usecases.md %}).

Enjoy reading and using HIFIS!

The HIFIS Team
