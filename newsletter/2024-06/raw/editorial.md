Dear HIFIS Community and Friends,

Are you working with scientific data? Or any of your colleagues? If so, you and your fellow colleague should not miss the virtual roadshow organized by the five Helmholtz Information and Data Science platforms HIFIS, Helmholtz AI, Helmholtz Imaging, HMC and HIDA! Spread the word!
This virtual event takes place on **June 26th 10 – 11:30 am**. Don't miss the opportunity and [**register here**](https://events.hifis.net/event/1609/registrations/1629/) - it's open and free!
Interact with representatives and scientists from each platform to gain insights into their offers.
Discover how the platforms can support and enhance your research.
And: Explore potential collaborations with other experts of the Helmholtz Association.

We are happy to announce that we've got a new spokesperson for HIFIS: Philipp Neumann took over in May 2024.
Read more about it in this issue of our newsletter, and also on our new education offers, progresses in cloud service legal framework and more.
Enjoy using our HIFIS services!
Make sure to spread the word and [subscribe to our newsletter](https://hifis.net/subscribe).

Finally, a reminder: If you are involved or otherwise interested in HIFIS, [**register here**](https://events.hifis.net/event/1219/registrations/) for the upcoming [HIFIS annual meeting in September at DLR, Cologne](https://events.hifis.net/event/1219/).
We welcome all interested people from Helmholtz and partners and look forward to seeing you there!

The HIFIS Team
