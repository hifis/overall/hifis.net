**First round of Helmholtz Incubator Software Award finalized**

The selection process for the inaugural [Helmholtz Incubator Software Award](https://os.helmholtz.de/en/open-research-software/helmholtz-incubator-software-award/) has been finalized, the winners in the three price categories will be announced soon.
The overwhelming response and high-quality submissions have encouraged us to launch a second round.
We anticipate to publish the new call by summer 2024. Stay tuned!

Register or update your software project at the [Helmholtz Research Software Directory (RSD)](https://helmholtz.software/) now:
This is an excellent opportunity for you to showcase your innovative software projects, improve referencability and receive recognition for your contributions.
An entry in RSD is precondition to identify potential future awardees.

The award is based on an initiative of [HIFIS](https://www.hifis.net/), [HIDA](https://www.helmholtz-hida.de/en/), [HiRSE_PS](https://www.helmholtz-hirse.de/) and [Helmholtz Open Science Office](https://os.helmholtz.de/).

[Read more...]({% post_url 2024/2024-06-11-sw-award-winners %})
