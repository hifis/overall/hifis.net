**Philipp Neumann is the new HIFIS Spokesperson**

Since May 2024, Neumann has been the new Head of IT at DESY, a position that includes the role as HIFIS Spokesperson.
His predecessor, Volker Gülzow, retired recently.
Gülzow tirelessly initiated and significantly contributed to the establishment of the HIFIS platform, making it a success from the start.

Neumann’s start coincides with an important phase for the HIFIS platform, as the new funding period begins in 2025.

We wish him much success in his new role and look forward to the impulses he will bring to the platform.

[Read more...]({% post_url 2024/2024-06-12-neumann %})
