**Helmholtz Cloud: The legal basis is in place**

After four years of hard work, the ruleset of the Helmholtz Cloud has taken a big leap forward.
HIFIS brought together experts from legal and tax law and engaged external consultants.
Helmholtz’s diversity, with two different ministries as funders and different legal forms, required careful research into the financing of the Helmholtz Cloud.
Now, 18 Helmholtz Centres, along with the head office, have reached an agreement on the rules for service providers and user groups in the Helmholtz Cloud.

[Read more...]({% post_url 2024/2024-04-30-cloud-ruleset %})
