**Join the TEACH 4 conference!**

Are you an educator, personnel developer, or simply interested in improving teaching and learning in the Helmholtz context?
This conference is for you: Join the fourth edition of the TEACH conference, and share knowledge, experiences and good practises!

[Read more here]({% post_url 2024/2024-05-13-teach-4 %}) and on the [Events page](https://events.hifis.net/e/teach4).
