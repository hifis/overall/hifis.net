---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #00285a}
</style>
<title>HIFIS News June 2024</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#platforms">News from the Helmholtz Platforms</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#00285a"><strong>News from HIFIS</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="Philipp_Neumann_162px.jpg" alt="Portait of Philipp Neumann, spokesperson HIFIS" border="0" align="left">
<div markdown="1">
{% include_relative raw/new_spokesperson.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="jonny-gios-SqjhKY9877M-unsplash_162px.jpg" alt="tiles" border="0" align="right">
<div markdown="1">
{% include_relative raw/legal_basis.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="platforms"><span style="color:#00285a"><span style="font-size:16px"><strong>News from the Helmholtz Platforms</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="giorgio-trovato-_XTY6lD8jgM-unsplash_162px.jpg"  alt="award" border="0" align="left">

<div markdown="1">
{% include_relative raw/software_award.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="Teach4_Indico_162px.jpg" alt="teach 4 logo" border="0" align="right">
<div markdown="1">
{% include_relative raw/teach4.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<div markdown="1">
{% include_relative raw/roadshow.md %}
</div>

<a href="https://events.hifis.net/event/1609/registrations/1629/"><img src="DataScience-Roadshow-indico-540px.jpg"  alt="roadshow" border="0"></a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
