**HIFIS at IT4science days 2023**

HIFIS was well-represented at the IT4science days in Berlin last September. Engaging with fellow IT professionals from diverse research centres invigorated us with fresh perspectives and strategies.

[Read more...]({% post_url 2023/2023-10-16-IT4science %})