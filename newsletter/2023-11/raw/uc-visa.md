**The VISA collaboration is using HIFIS services**

VISA is a platform to facilitate open science, where users can share workspaces in a browser, enabling remote collaboration and data analysis on facility resources from anywhere. 
VISA is adopted by a growing number of light and neutron source facilities across Europe, organised under the so-called "VISA collaboration". They are since this summer using HIFIS resources, fostering a seamless partnership across borders.

[Read more...]({% post_url 2023/2023-10-26-VISA %})