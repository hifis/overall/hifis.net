**Preserving Helmholtz Codebase legacy**

Our commitment to preserving research software legacy is now embedded in the Software Heritage Archive! All the public repositories of the Helmholtz Codebase are since September automatically and regularly harvested there.

[Read more...]({% post_url 2023/2023-10-18-software-heritage %})