**New Release of Research Software Directory**

The Helmholtz Research Software Directory ([https://helmholtz.software](https://helmholtz.software)) has received a major update to version 2 of the [RSD](https://research-software-directory.org). The update includes redesigned overview pages for software, projects and organisations, that are visually more appealing and provide enhanced filtering options.  

We are also pleased to announce that the number of software submissions to the RSD has been steadily increasing since its launch in February.

[Read more...]({% post_url 2023/2023-11-09-rsd-update %})
