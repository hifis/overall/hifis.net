---

---

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="color-scheme" content="light only">
<style>
    a { color: #00285a}
</style>
<title>HIFIS News November 2023</title>

{% include_relative html_bits/page_intro.htm %}

<a style="text-decoration: none; " href="#hifisnews">News from HIFIS</a><br>
<a style="text-decoration: none; " href="#incubator">News from Helmholtz Incubator</a><br>
<a style="text-decoration: none; " href="#jobs">Data Science Jobs at Helmholtz</a>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/section_intro.htm %}

{% include_relative html_bits/section_separator.htm %}

<span style="font-size:18px" id="hifisnews"><span style="color:#00285a"><strong>News from HIFIS</strong></span></span><br>

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="RSD-statistics.png" alt="Evolution of software entries of Helmholtz RSD" border="0" align="right" />
<div markdown="1">
{% include_relative raw/news-rsd-release.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="{% link assets/img/third-party-logos/SWH-logo.png %}" alt="Software Heritage logo, collect, preserve, share" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-software-heritage.md %}
</div>


{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 20%;width: 108px;" src="{% link assets/img/services/visa-logo-blue.png %}" alt="VISA logo" border="0" align="right" />

<div markdown="1">
{% include_relative raw/uc-visa.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 30%;width: 162px;" src="{% link assets/img/jumbotrons/camylla-battani-ABVE1cyT7hk-unsplash.jpg %}" alt="many hands joining" border="0" align="left" />
<div markdown="1">
{% include_relative raw/news-all-hands.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="{% link assets/img/jumbotrons/mikael-kristenson-3aVlWP-7bg8-unsplash.jpg %}" alt="people attending a conference" border="0" align="right" />
<div markdown="1">
{% include_relative raw/news-it4science.md %}
</div>

{% include_relative html_bits/section_separator.htm %}

<div style="line-height:24px " id="incubator"><span style="color:#00285a"><span style="font-size:16px"><strong>News from the Helmholtz Incubator</strong></span></span></div>

<img style="margin: 20px 20px 20px 20px;max-width: 20%;width: 108px;" src="incubator_logo.png" alt="Logo of Helmholtz Incubator" border="0" align="left" />

<div markdown="1">
{% include_relative raw/news-incubator-academy.md %}
</div>

{% include_relative html_bits/section_subseparator.htm %}

<img style="margin: 20px 20px 20px 20px;max-width: 40%;width: 216px;" src="{% link assets/img/posts/2023-08-14-podcast-softwareaward/HelmholtzPreis.png %}" alt="Code for though with Uwe Konrad" border="0" align="right" />

<div markdown="1">
{% include_relative raw/news-podcast-sw-award.md %}
</div>

{% include_relative html_bits/section_outtro.htm %}

{% include_relative html_bits/page_outtro.htm %}
