window.onscroll = function() {scrollFunction()};


/*
    * Start shrinking the header after scrolling 20% of the window height.
    * This is done by adding the " shrink" class to the header.
    */
function scrollFunction() {
    const canaryClass = "shrink";

    var threshold = window.innerHeight * 0.2; // 20 % of the window
    if (document.body.scrollTop             > threshold
    ||  document.documentElement.scrollTop  > threshold) {
        document.getElementById("header").classList.add(canaryClass);
    } else {
        document.getElementById("header").classList.remove(canaryClass);
    }
}

function scrollableHeader() {
    document.getElementById("header").classList.toggle("scrollable-header");
    document.body.classList.toggle("no-scroll");
}

function toggleMenu() {
    document.getElementById("menu").classList.toggle("nav-expanded");
    menuIconClassList = document.getElementById("menu-button").firstElementChild.classList;
    if (menuIconClassList.contains("fa-bars")) {
        menuIconClassList.remove("fa-bars");
        menuIconClassList.add("fa-x");
    } else {
        menuIconClassList.remove("fa-x");
        menuIconClassList.add("fa-bars");
    }
}
