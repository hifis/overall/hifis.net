FROM ruby:latest

ARG DUID=1000
ARG DGID=1001

EXPOSE 4000

RUN apt-get update && apt-get -y install build-essential zlib1g-dev ffmpeg
RUN gem install jekyll bundler
RUN git config --global --add safe.directory /app

WORKDIR /app

CMD bundle install && bash scripts/create_jumbotrons.sh assets/img/jumbotrons/ && bundle exec jekyll serve --future --host 0.0.0.0
