---
title: Media & Outreach materials
title_image: default
layout: default
additional_js: frontpage.js
redirect_from: pr
excerpt:
    HIFIS Media & outreach materials.
---

{:.treat-as-figure}
{:.float-right}
{:.p-5}
![person on the right and person on the left talk about starting a project and how HIFIS can help with that]({{ site.directory.images | relative_url }}/hifisfor/poster-1.svg)
Lorna Schütte, CC-BY-NC-SA

### Outreach material
* Meet __Sam, the Scientist__: HIFIS for Scientific Workflows
  * Poster: [svg]({{ site.directory.images | relative_url }}/hifisfor/HIFIS_poster_claim_contact.svg), [pdf]({{ site.directory.images | relative_url }}/hifisfor/HIFIS_poster_claim_contact.pdf)
  * [Postcard / small Poster]({{ 'media/Postcard_A6_claim.svg' | relative_url }})
  * [Video]({{ site.directory.videos | relative_url }}/video_full.mp4)
* Overview of HIFIS: Digital Services for Helmholtz & Partners
  * [English Version]({{ 'media/HIFIS_overview_en.pdf' | relative_url }})
  * [German Version]({{ 'media/HIFIS_overview_de.pdf' | relative_url }})
* [Helmholtz Incubator Brochure]({{ 'media/Helmholz-Incubator-Folder_2022.pdf' | relative_url }})

### HIFIS logos
* HIFIS logos can be reused under the [CC-BY 4.0 license](https://creativecommons.org/licenses/by/4.0/legalcode)
    * HIFIS logo (blue): [svg]({{ '/assets/img/HIFIS_Logo_short_RGB_cropped.svg' | relative_url }}), [png]({{ '/assets/img/HIFIS_blue_96dpi.png' | relative_url }})
    * HIFIS logo (white): [svg]({{ '/assets/img/HIFIS_white.svg' | relative_url }}), [png]({{ '/assets/img/HIFIS_white_96dpi.png' | relative_url }})
* Guidelines for the use of our logos and more can be found in our [Corporate Design guidelines]({% link corporatedesign.md %}).

{:.float-right}
{:.p-5}
![]({{ '/assets/img/illustrations/undraw_listening_re_c2w0.svg' | relative_url }})

### Podcast Episodes
* Software Preisauschreibung bei Helmholtz - mit Dr. Uwe Konrad.<br>
Code for Thought Podcast, Season 5, Episode 24 (July 2023)
  * [Blogpost]({% post_url 2023/2023-08-14-podcast-award %}) & [direct link](https://codeforthought.buzzsprout.com/1326658/13276955-de-extra-software-preisauschreibung-bei-helmholtz-mit-dr-uwe-konrad) <i class="fas fa-headphones"></i>
* Bringt uns Programmieren bei - mit Fredo Erxleben.<br>
Code for Thought Podcast, Season 5, Episode 19 (June 2023)
  * [Blogpost]({% post_url 2023/2023-06-29-podcast-education %}) & [direct link](https://codeforthought.buzzsprout.com/1326658/13072511-de-bringt-uns-programmieren-bei-mit-fredo-erxleben) <i class="fas fa-headphones"></i>
* HIFIS. Helmholtz Resonator Podcast, Episode 172 (October 2021)
  * [Blogpost]({% post_url 2021/10/2021-10-18-podcast %}) & [direct link](https://resonator-podcast.de/2021/res172-hifis) <i class="fas fa-headphones">
