---
date: 2024-11-30
service: cloud
---

## Quality Criteria to evaluate the Service Portfolio
The documented baseline of defined criteria makes the quality of the service portfolio as a whole measurable and comparable. 