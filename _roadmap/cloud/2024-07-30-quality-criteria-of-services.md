---
date: 2024-07-30
service: cloud
---

## Quality Criteria assist to monitor Cloud Services
To ensure that cloud services meet high quality standards above what is absolutely required during the Service Onboarding, the HIFIS Cloud team evaluates criteria to measure service quality during operation and identified five key fields, [read more](https://hifis.net/news/2024/07/31/cloud-quality-areas.html).