---
date: 2024-08-15
service: cloud
---

## Booking of Resources as 1st Customized Function of the Cloud Portal
As a logged-in user one can book, manage and delete resources without writing emails to the help desk. The entire process is handled in the Cloud Portal and the Helmholtz Cloud Agent, and transmitted to the service provider without any manual steps.