---
date: 2024-10-15
service: cloud
---

## All Helmholtz Cloud Nextclouds merge to one single view
In cooperation with Nextcloud and initiated by HIFIS, Nextcloud has now rolled out an update that enables federation: Several Nextcloud instances can join together so that the user sees them all together in one view.  