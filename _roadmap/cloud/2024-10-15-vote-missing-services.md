---
date: 2024-10-15
service: cloud
---

## Survey on Most Important Missing Services
A short survey reveals a ranking of which collected services ideas are most relevant to users.