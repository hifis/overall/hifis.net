---
date: 2023-08-31
title: Tasks in Aug 2023
service: cloud
---

## Access List DB manages the resource bookings in the Helmholtz Cloud
The [Helmholtz Cloud Portal](https://helmholtz.cloud/) allows to book resources within specific Helmholtz Cloud Services. In order to know who is allowed to book which type of resource in which capacity, an access list is programmed in the service database Plony. This brings together the information about the resource booking in the Cloud Portal and the group membership in the Helmholtz AAI.
