---
date: 2024-09-30
service: cloud
---

## Bachelor thesis implementing resource booking in Plony successfully completed
Service providers can now configure all aspects around resource booking in Plony. This includes the content of the resource booking form displayed in Cloud Portal, how many resources are offered as well as access control to them. The modeling of the corresponding process as well as the implementation in Plony was subject to a bachelor thesis written at HZB.