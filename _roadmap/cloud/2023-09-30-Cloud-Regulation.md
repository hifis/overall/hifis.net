---
date: 2023-09-20
title: Tasks in Sep 2023
service: cloud
---

## Submission of the Cloud Regulations to the Helmholtz Assembly of Members 
The draft rules comes to an accepted solution for all touched diciplines: general legal, VAT, and state aid topics. Stakeholders such as data protection officers and works councils are informed. The Assembly of Members of the Helmholtz Association have received the document for signature, so that the use of technical resources within the context of the Helmholtz Cloud are regulated.

