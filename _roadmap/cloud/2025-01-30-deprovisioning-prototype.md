---
date: 2024-11-01
service: cloud
---

## Automated Deprovisioning of Users in Plony Database 
As a blueprint, Plony Database, one of the cloud core components, is going to be the first service in where users are automatically deprovisioned based on the Helmholtz-AAI information about deleted or deactivated accounts. 