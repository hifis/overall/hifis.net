---
date: 2022-08-31
title: Tasks in August 2022
service: cloud
---

## Onboarding Process of new Cloud Services completely integrated in Plony
All steps of the Onboarding Process for new Cloud Services are completely integrated into [Plony](https://plony.helmholtz.cloud/), the Plone-based database. This enables Service Providers and HIFIS administrators to maintain the Service Catalogue online and collaboratively. The Service Description is automatically transferred to Cloud Portal.
