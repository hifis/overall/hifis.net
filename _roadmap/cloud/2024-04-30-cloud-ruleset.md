---
date: 2024-04-30
service: cloud
---

## Helmholtz Cloud Ruleset becomes valid
Two important agreements of the Helmholtz Cloud are finalized: The Helmholtz Cloud Ruleset regulates the roles, duties and tasks for providing and using the services. It is flanked by the Joint Controller Agreement of the Core Components that covers the GDPR requirements. [Find more information here.](https://hifis.net/news/2024/04/30/cloud-ruleset.html) 