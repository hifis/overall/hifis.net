---
date: 2024-11-06
service: cloud
---

## Draft Process for data processing agreement (DPA): Feedback by Helmholtz Data Protection Officers
HIFIS is developing a draft process to simplify the conclusion of the necessary data processing agreements (DPAs) within the Helmholtz Cloud. The DPAs supplements the existing Joint Controller Agreement and must be must be concluded between the service providers and using centres. This draft process is submitted to the Helmholtz Data Protection Officers (AK Datenschutz) for their review and feedback.