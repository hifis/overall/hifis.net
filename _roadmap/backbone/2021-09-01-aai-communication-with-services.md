---
date: 2021-09-01
title: Tasks in September 2021
service: backbone
---

## AAI: Communication with services
Developed concept and alpha implementation for automated communication with services, for example using
Local Agent or via Cloud Portal. This requires cooperation with the
Service Integration working group of the Cloud Cluster.



