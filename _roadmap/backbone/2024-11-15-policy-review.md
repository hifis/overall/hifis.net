---
date: 2024-11-15
service: backbone
---

## AAI Policy Review
With experiences made during the last 2-3 years (annual housekeeping, user deprovisioning, adoption of new use cases) the AAI policies will be reviewed and potentially updated in close collaboration with all other clusters and AAI participants (e.g. NFDI)
