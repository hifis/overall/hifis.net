---
date: 2023-04-01
service: backbone
---

## AAI: HIFIS members are part of the 3rd AARC proposal
HIFIS Backbone members are participating in the EU project proposal for the third AARC project, which will update the globally accepted AARC Blueprint Architectures, Policies, and Guidelines.
The participation of HIFIS partners will intensify useful feedback in both directions between the global AAI community and HIFIS.
