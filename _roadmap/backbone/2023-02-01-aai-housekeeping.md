---
date: 2023-02-01
service: backbone
---

## AAI: Yearly housekeeping
After ramp-up phase, and going towards operational phase, the components of Helmholtz AAI will undergo a regular updating and housekeeping process.
Amongst these, the owners of registered services as well as the managers of registered Virtual Organisations (VO) will be contacted and requested for information confirmation or updates.
Further updates and regular review processes are to be defined in the upcoming Policy Review Process.
