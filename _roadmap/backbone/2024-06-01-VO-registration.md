---
date: 2024-06-01
service: backbone
---

## Updating and streamlining the VO registration process
Based on a review of the overall process and policies involved in VO registration, 
implementation of the process in plony database, 
and improving interconnection with AAI community proxy (Unity IdM), 
the VO registration will be streamlined and made more transparent to users.
