---
date: 2024-10-01
service: backbone
---

## AAI: MFA honoring
An increasing number of IdPs supports MFA for federated login.
To improve user experience, i.e. avoiding to bother users with multiple MFA entering, honoring MFA in the login chain is being implemented. 
Corresponding documentation will be provided and signalling the MFA status will be prepared.
