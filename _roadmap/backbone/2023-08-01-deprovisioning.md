---
date: 2023-08-01
service: backbone
---

## AAI: Deprovisioning of inactive / non-present users
The technical tooling of asking IdPs to report the status of a specific user via Attribute Query, is supported by the Helmholtz Community AAI.
For summer, we plan to put the automated deprovisioning process into action for users that have not logged in to Helmholtz AAI by more than a defined grace time, either by a) querying IdPs which support this until then, or b) querying users via email to log in again.
The deprovisioning information is then forwarded to connected cloud services.
