---
date: 2023-09-01
service: backbone
---

## AAI: Integrate most relevant federations, based on negotiated use cases (pilot)
Any completely transparent interconnection of Helmholtz AAI with other federations, such as EGI Checkin, ELIXIR/Lifescience, or Indigo IAM, is notoriously problematic due to many degrees of freedom of such implementations (and thus potential conflicts) in terms of user lifecycle management, authorisation management, group memberships, applying policies and more.
In coordination with specific user groups using infrastructures from other federations and communities, we will define specific technical and administrative procedures to enable the negotiated use cases while minimising the abovementioned problems.
