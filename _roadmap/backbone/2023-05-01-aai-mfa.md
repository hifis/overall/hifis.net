---
date: 2023-05-01
service: backbone
---

## AAI: Multi Factor Authentication (MFA)
It is planned to enforce MFA in administrative endpoints of the central community AAI (Unity IdM), i.e., the /console and /oauth-home (userhome of oauth clients) endpoints.
It will be made optional on normal user's /home /upman /oauth2 /saml-idp endpoints and recommended for VO admins to use it.
Corresponding documentation will be provided and signalling the MFA status will be prepared.
