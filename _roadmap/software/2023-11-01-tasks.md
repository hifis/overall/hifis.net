---
date: 2023-11-01
title: Tasks in November 2023
service: software
---

## Helmholtz Software Award

For the first time, an award for high-quality research software will be granted in Helmholtz.
