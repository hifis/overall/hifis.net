---
date: 2023-05-01
title: Tasks in May 2023
service: software
---

## RSE Bootcamp

In spring and summer 2023, scientists will have the opportunity to be taught
the combined HIFIS course portfolio in a joint RSE Bootcamp organized with HIDA.
