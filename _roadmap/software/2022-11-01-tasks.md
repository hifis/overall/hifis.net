---
date: 2022-11-01
title: Tasks in November 2021
service: software
---

## Empower CI/CD #3 - Support Windows in GitLab CI

So far the Helmholtz Codebase service does not support the Windows operating
system for executing jobs.
As part of this milestone the Helmholtz Codebase service will support the Windows
operating system to run CI jobs.

## Scale with rising usage figures through advanced maintenance automation

As part of this milestone it should be possible to

* test the core functionality of the Helmholtz Codebase service automatically,
* automatically setup a test infrastructure via OpenStack.
