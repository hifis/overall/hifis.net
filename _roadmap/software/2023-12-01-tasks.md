---
date: 2023-12-01
title: Tasks in December 2023
service: software
---

## Empower CI/CD part two

New functionalities will be enabled by a closer connection
of the services Helmholtz Codebase and Mattermost with the
Helmholtz Cloud Portal.
This will, for instance, add support for VOs or
allow the automatic deprovisioning of users.
