---
date: 2024-05-01
service: overall
---

## Philipp Neumann is the new HIFIS Coordinator
By taking over his role as DESY Head of IT at the beginning of May 2024, he also became the spokesperson for the HIFIS platform. [Read more...]({% post_url 2024/2024-06-12-neumann %})