---
date: 2023-07-01
service: overall
---

## Incubator Workshop: Presentation of future developments
In the Helmholtz Incubator Workshop to be held in July,
HIFIS will present the envisioned future developments,
especially in the light of the advises given
during [HIFIS evaluation]({% post_url 2022/2022-12-12-evaluation-update %}).
