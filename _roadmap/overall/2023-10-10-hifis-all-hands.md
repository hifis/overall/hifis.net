---
date: 2023-10-10
service: overall
---

## HIFIS All Hands Meeting
Having worked out the future plans for HIFIS and possibly with a first feedback from the Helmholtz Assembly of Members to be held in September,
we will gather in an all-hands meeting in or nearby Dresden / HZDR.
HIFIS stakeholders such as [SAB](https://hifis.net/sab) and [Federation Board](https://hifis.net/fedboard) will be invited.
