---
date: 2024-03-25
service: overall
---

## Finalization of Incubator Framework 2.0
During winter, the "Incubator Framework 2.0" will be finalized.
Future development plans will be carved out and prepared for presentation to the April's Helmholtz Assembly of Members.
This is a follow-up to the preceding decision from October 2023.
