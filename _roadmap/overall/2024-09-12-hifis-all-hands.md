---
date: 2024-09-12
service: overall
---

## HIFIS All Hands Meeting
The annual all-hands meeting will take place in September, visiting our HIFIS team member DLR in Cologne.
Further details will be published on the [events page](https://events.hifis.net/event/1219/).
HIFIS stakeholders such as [SAB](https://hifis.net/sab) and [Federation Board](https://hifis.net/fedboard) will be invited.
