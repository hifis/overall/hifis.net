---
date: 2020-03-31
title: Tasks in March 2020
service: overall
---

## Reporting for the first year of HIFIS

The first steps, progress and ongoing works of HIFIS Cloud, Backbone, and Software Clusters have been reported in the first annual report of HIFIS. This report has been improved and approved by the ICT Federation Board and further served as a basis for feedback by the Scientific Advisory Board in April.
