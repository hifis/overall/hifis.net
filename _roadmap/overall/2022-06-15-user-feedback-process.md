---
date: 2023-06-01
service: overall
---

## Follow-up process on user feedback
HIFIS provides numerous channels that already allow users and user groups to funnel their feedback on HIFIS services.
The tools include
the single point-of-contact [Helpdesk](mailto:support@hifis.net),
direct feedback in the [Helmholtz Cloud Portal](https://helmholtz.cloud/services/),
[surveys](https://hifis.net/survey2021),
the Helmholtz Cloud [Service Operation KPI](https://hifis.net/doc/cloud-service-kpi/),
and many more.
Until summer, we will streamline the processes to follow-up on user queries, including selection and priorisation.
We will decide on potential additional or altered formats to systematically obtain user feedback.
Further, the reporting (KPI) will be adapted,
moving a bit away from reflecting the build-up of services,
towards the representation of user acceptance and user satisfaction.
