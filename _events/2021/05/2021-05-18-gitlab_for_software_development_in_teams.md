---
title: "GitLab for Software Development in Teams"
layout: event
organizers:
  - schlauch
type: workshop
start:
  date:   "2021-05-18"
  time:   "09:00"
end:
  date:   "2021-05-20"
  time:   "13:00"
location:
  campus: "Online"
excerpt:  "This workshop provides an introduction into software development for teams using Git and GitLab."
registration_period:
  from:   "2021-04-26"
  to:     "2021-05-07"
registration_link:  "https://events.hifis.net/event/103/"
fully_booked_out:  "False"
---
## Goal

This workshop teaches you how to organize your development process using Git and GitLab to better collaborate with others on a software project.


## Content

This workshop will cover the the following topics:

 - Practical implementation of development processes using Git and GitLab
 - Git advanced concepts such as branching, merging, rebasing
 - GitLab advanced concepts such as merge requests, issue tracking, build pipelines

Particularly, the workshop includes a team exercise which allows you to work through the development process of our example software project.

Please see the [workshop curriculum](https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/workshop-materials#curriculum) for further details.

## Requirements

- Basic Git skills are required. A good and quick tutorial can be found in the [Software Carpentry's Git Novice episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
 - Participants require a computer equipped with a recent Git command line client, a modern Web browser, and a text editor. We will provide more detailed setup information before the workshop.

We are looking forward to seeing you! 
