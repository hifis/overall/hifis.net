---
title: "Test Automation with Python (rescheduled)"
layout: event
organizers:
  - schlauch
type: workshop
start:
  date:   "2021-11-23"
  time:   "09:00"
end:
  date:   "2021-11-24"
  time:   "13:00"
location:
  campus: "Online"
excerpt:  "Learn to test your Python code effectively."
registration_period:
  from:   "2021-10-25"
  to:     "2021-11-14"
registration_link:  "https://events.hifis.net/event/155/"
fully_booked_out:  "False"
redirect_from:
  - events/2021/11/09/test_automation_with_python
---
## Goal

You learn the basics of efficient testing in Python. In addition, we show you how to apply them practically in your Python project.


## Content

This workshop will cover the the following topics:
- Introduction to test automation
- `pytest` basics
- Test isolation using `mock`
- Code coverage analysis using `coverage.py`
- Run tests via a GitLab CI build pipeline

You learn to apply the presented strategies on the basis of a consistent code example.

Please see the [workshop curriculum](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/workshop-materials#curriculum) for further details.

## Requirements

- Good Python programming skills are required.
- Participants require a computer equipped with a modern Web browser and a recent Python environment.
  We will provide more detailed setup information before the workshop.

We are looking forward to seeing you!

