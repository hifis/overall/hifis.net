---
title: The Carpentries Instructor Training
layout: event
organizers:
  - huste
  - dolling
type:   workshop
start:
    date:   "2020-01-20"
end:
    date:   "2020-01-21"
excerpt:
  The Carpentries Instructor Training will provide the participants with the
  skills and information needed to become a certified Carpentries instructor.

---
## Content
The Carpentries[^1] is an organization which aims to be the
leading inclusive community teaching data and coding skills.
The Carpentries Instructor Training will provide the participants with the
skills and information needed to become a certified Carpentries instructor.
You can find further information about the process and content of this
training in the course material[^2].

{:.treat-as-figure}
The Carpentries logo
[!["The Carpentries Logo"]({{ site.directory.images | append: 'third-party-logos/TheCarpentries.svg' | relative_url }} "The Carpentries Logo")][1]

This training will be held in two different Helmholtz locations at the same
time: at GFZ in Potsdam and at HZDR in Dresden.
Both Helmholtz centers are currently a member organization of The Carpentries.
The methods and approaches taught will enable us to offer high-quality courses
in 2020 and beyond.

## References
[^1]: [About page of The Carpentries website][1]
[^2]: [The Carpentries (Software, Data, and Library Carpentry) instructor training course material][2]

[1]: https://carpentries.org/about/
[2]: https://carpentries.github.io/instructor-training/
