---
title: Software Carpentry Workshop
layout: event
organizers:
  - huste
  - erxleben
lecturers:
  - erxleben
  - huste
  - Steinbach, Peter
type:   workshop
start:
    date:   "2020-03-31"
end:
    date:   "2020-04-01"
location:
    campus: hzdr
    room:   "Building 801, Room P142"
fully_booked_out: true
registration_link: https://events.hifis.net/event/5/registrations/4/
waiting_list_link: https://events.hifis.net/event/8/registrations/5/
registration_period:
    from:   "2020-01-20"
    to:     "2020-03-27"
excerpt:
    "This basic Software Carpentry workshop will teach Shell, Git
    and Python for scientists and PhD students."
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

A Software Carpentry workshop is conceptualized as a two-day event that covers
the basic tools required for a research software workflow:

* The _Shell_ as a foundation for the following tools
* Employing _Git_ as version control system (VCS)
* Introduction into the _Python_ programming language

Details and workshop materials can also be found directly at the
[Software Carpentries' lessons overview][swc-lessons].


## Requirements

Neither prior knowledge nor experience in those tools is needed.
Participants are asked to bring their own computer on which they can install
software.
It is recommended to read and follow the instructions on [how to set up the tools][setup-tools] for the workshop before the event.

[swc-lessons]: https://software-carpentry.org/lessons/
[setup-tools]: https://hifis.gitlab.io/2020-03-31-hzdr/#setup

