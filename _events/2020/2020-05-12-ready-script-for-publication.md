---
title: "Bring Your Own Script and Make It Ready for Publication"
layout: event

# workshop, seminar, lecture or discussion
type: workshop

# IDs from https://codebase.helmholtz.cloud/hifis/software.hifis.net/blob/master/_data/hifis_team.yml
organizers:
  - schlauch
lecturers:
  - schlauch
  - dolling 
  - Leinweber, Katrin
  - huste

start:
    date:   "2020-05-12"
    time:   "09:00"
end:
    date:   "2020-05-12"
    time:   "17:00"
location:
    campus:  "Online"
#    room:   "Online"

# URL or mailto:organizer@campus.edu?subject=...&body=...
# with the text in ... encoded using for example https://www.urlencoder.org/
# Both are optional but at least ?subject=... is recommended to enable mail filtering
registration_link: https://events.hifis.net/event/12/registrations
waiting_list_link: https://events.hifis.net/event/14/registrations
fully_booked_out: true

registration_period:
    from: "2020-02-01"
    to: "2020-04-30"

excerpt: "We will help you polish an existing software project to a publication-ready state:\n
<ul>\n
    <li>Reviewing installation instructions and documentation</li>\n
    <li>Helping you decide on a license</li>\n
    <li>Preparing the required publication steps and more…</li>\n
</ul>\n
Any programming language is welcome!"
---

## Goal
In this workshop you learn about the basic steps to prepare your code for
sharing with others and make it ready for citation in a research paper.

## Content

We will provide you with actionable advice about how to polish your software 
project before publishing it or submitting it alongside a publication.
This includes:

1. Put your code under version control
2. Clean up your code
3. Provide proper documentation
4. Add a suitable Open Source license
5. Mark the stable version of your code
6. Make your code citable

We provide an overview about these topics in two theory sessions in the morning
and in the afternoon. In between, you can work on your software project and
improve it. The instructors are available and support you by
answering your questions and providing feedback.

## Prerequisites

- Ideally, you already have a software project which you want to improve.
Alternatively, we prepared tasks on which you can work during the practice sessions.
- Basic Git skills are required.
A good and quick tutorial can be found in the 
[Software Carpentry's "Git Novice" episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
- You require your laptop with your project setup, plus the Git command line or a graphical client,
and a modern web browser.
