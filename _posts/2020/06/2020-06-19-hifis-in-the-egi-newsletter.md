---
title: "HIFIS in the EGI newsletters"
title_image: default
date: 2020-06-08
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt_separator: <!--more-->
lang: en
lang_ref: 2020-06-19-hifis-in-the-egi-newsletter
---

_Helmholtz Federated IT Services - Promoting IT based science at all levels_,
is the title of our article that appeared in the newsletter of our friends 
from the EGI Federation. In the EGI Newsletters, we give an overview of the
new HIFIS platform.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://preview.mailerlite.com/s9b7l1/1435456442688607486/y2j8/">
  <i class="fas fa-external-link-alt"></i> Read more
</a> {% comment %} The link is only to the newsletter, the more detailed article has gone offline, as well as all the articles from older issues. Reported bug to EGI.eu (Sophie). {% endcomment %}
