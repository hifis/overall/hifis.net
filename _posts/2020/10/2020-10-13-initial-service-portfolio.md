---
title: "Selected Services for Initial Helmholtz Cloud Service Portfolio"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
date: 2020-10-13
authors:
  - spicker
  - schollmaier
layout: blogpost
categories:
  - News
excerpt_separator: <!--more-->
---

**The selection process for the initial Helmholtz Cloud service portfolio has been successfully completed.**

At the end of this process, 38 services offered by nine Helmholtz Centers were selected. Considering that some services are provided by several centers, the initial service portfolio includes 21 different services.
<!--more--> 

# The Initial Service Portfolio
In summary, the following services form the initial service portfolio of the Helmholtz Cloud:

| Services | Service Provider | Main Service Category |
| :--| :-- | :-- |
| **OpenStack** |   Jülich, Jülich (HDF), KIT, DKFZ   |   Infrastructure Service |
| **Storage (HDF)**   |   Jülich, DESY   |   Infrastructure Service |
| **HAICORE (HAICU, HIP)**   |   KIT, Jülich   |   Infrastructure Service |
| **Singularity**   |   KIT, Jülich   |   Infrastructure Service |
| **Docker**   |   DESY   |   Infrastructure Service |
| **GPU Compute Service**   |   Jülich, HZDR   |   Infrastructure Service |
| **AWI Marketplace**   |   AWI   |   Infrastructure Service |
| **GitLab**   | HZDR, KIT, Jülich, GEOMAR &nbsp;&nbsp;&nbsp;&nbsp;   |   Science/Community Service |
| **JupyterHub**   |   Jülich, DESY, DKFZ, HMGU   |   Science/Community Service |
| **B2Share (Invenio)**   |   Jülich   |   Science/Community Service |
| **JupyterHub Notebooks on HPC**   |   KIT   |   Science/Community Service |
| **ODV**   |   AWI   |   Science/Community Service |
| **RODARE**   |   HZDR   |   Science/Community Service |
| **Ocean and Climate Sensor Management** &nbsp;&nbsp;&nbsp;&nbsp;   |   AWI   | Science/Community Service |
| **Rocket.Chat**   |   Jülich   |   Collaboration Service |
| **Zammad**   |   HZDR   |   Collaboration Service |
| **Mattermost**   |   HZDR   |   Collaboration Service |
| **Nextcloud (OnlyOffice)**   |   KIT, HZB, DESY   |   Collaboration Service |
| **LimeSurvey**   |   HMGU, DKFZ   |   Collaboration Service |
| **Redmine**   |   HZDR, HMGU   |   Collaboration Service |
| **ShareLaTex**    |   HZDR   |   Collaboration Service |

# Service integration of the initial service portfolio
The points obtained in the service selection process determine the ranking for service integration. The ranking roughly reflects the time required by the service providers as  necessary preparation time (service readiness) for the services. A short preparation time was weighted comparatively high in the initial service portfolio; this weighting will be adjusted in future service selections. 
 
The next step is to establish a roadmap for the service integration. Our goal is to have the first services available on the Technical Platform (pilot version) by the end of the year.
 
# Further development of the service portfolio
In parallel to the integration of the initial service portfolio, those services that have achieved a lower score in the service selection process will be reassessed. The reason for low scores was often that services require a time-consuming preparation before they can be offered in the Helmholtz Cloud. In some cases, not all necessary information about the services were available, or internal processes within the center delayed a short-term provision.

<br>

<a type="button" class="btn btn-outline-primary btn-lg" href="https://hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/#final-results-of-service-selection-process">
  <i class="fas fa-external-link-alt"></i> Read more in the Technical Documentation
</a>

