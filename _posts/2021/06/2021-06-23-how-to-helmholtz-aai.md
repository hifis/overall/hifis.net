---
title: "Helmholtz ID / AAI login to Helmholtz Cloud Services"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2021-06-23
authors:
  - "Uwe Jandt"
layout: blogpost
categories:
  - Tutorial
tags:
  - Helmholtz AAI
excerpt: >
    Pictured tutorial on usage of Helmholtz ID / AAI login and registration procedures for Helmholtz Cloud Services, using the HIFIS Events Management Platform at <a href="https://events.hifis.net">events.hifis.net</a> as an example.
---

# Easy login via your username at your home institute with the Helmholtz ID / AAI

## Click-by-Click Tutorial

Like for all [HIFIS]({% link index.md %}) and [Helmholtz Cloud Services](https://helmholtz.cloud/services), you can use your home institution’s login to use the service seamlessly.
This is managed via the [Helmholtz ID / AAI]({% link services/backbone/aai.md %}) (Authentication and Authorisation Infrastructure).

**Open the service** &mdash; in this example the HIFIS Event Management Platform &mdash; in your browser.
Do so by [locating it in the Helmholtz Cloud Portal](https://helmholtz.cloud/services?search=hifis%20events&serviceDetails=svc-ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b) and clicking on "Go to service", or
direct access in the browser via <https://events.hifis.net>.

{:.treat-as-figure}
![Cloud portal link to service]({% link assets/img/how-to-aai/events.hifis.net/0_cloud_portal.png %})

---

Click on **Login** at the top right.

{:.treat-as-figure}
![Login button on events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/1_events_login.png %})

---

In some cases, you need to additionally click on **Helmholtz ID / AAI**.
<i>This step is mostly skipped.</i>

{:.treat-as-figure}
![Keycloak login page, click on Helmholtz ID / AAI]({% link assets/img/how-to-aai/events.hifis.net/2_keycloak.png %})

---

**Select your home institution**, i.e. the institution where you already have your home account.
You can scroll down or use filtering, e.g. <i>helmholtz</i> or <i>desy</i>.
The example is about DESY.
You have to find your own Helmholtz Centre or institution.

{:.treat-as-figure}
![Choice of institutions in Unity]({% link assets/img/how-to-aai/events.hifis.net/3_unity_institutions.png %})

---

Can't find your institution or it is not supported?
As a fallback, a **login from ORCID, Github or Google** can be performed.
Select it accordingly.

{:.treat-as-figure}
![Choice of institutions in Unity]({% link assets/img/how-to-aai/events.hifis.net/3_unity_orcid.png %})

---

Provide your **username, passphrase** of your home institution. There is no new password or username required.

{:.treat-as-figure}
![Login at home IdP]({% link assets/img/how-to-aai/events.hifis.net/4_home_idp.png %})

---

**Accept data transmissions** from your home institution's identity provider (IdP) to Helmholtz AAI, and further to events.hifis.net.

{:.treat-as-figure}
![Consent collection]({% link assets/img/how-to-aai/events.hifis.net/5a_consents_collected.png %})

---

**Register at Helmholtz ID / AAI**, if you log in the first time.
* Click on **Register**.
* Please check your name and mail address.
* Read and accept the AAI usage policies.   
    You may choose to remember your decision, so you won't be bothered again.  
    If you later decide otherwise, you can revoke consent and delete stored data [here](https://login.helmholtz.de/home/).
* Click on **Submit**.

{:.treat-as-figure}
![Unity Registration]({% link assets/img/how-to-aai/events.hifis.net/5b_unity_registration.png %})

---

**Continue with normal log-in** and enjoy access to the Helmholtz Cloud Service(s)!

{:.treat-as-figure}
![Successfully logged in to events.hifis.net]({% link assets/img/how-to-aai/events.hifis.net/6_events_logged_in.png %})

---

### Having problems with login to a Cloud Service?

If you have persisting problems, please **contact us at <support@hifis.net>**, providing:
* The exact service (and possibly sub-page) you want to access,
* The exact error your receive,
* The URL / address of the error page,
* The time point of your (failed) login attempt.

---

## Changelog

- 2022-12-13 Removed some outdated or irrelevant information, some formulation fixes.
- 2024-05-03 Shorten intro text and remove accordion.
- 2024-09-27 Add Helmholtz 'ID' at beginning
