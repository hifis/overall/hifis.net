---
title: "Helmholtz Cloud Survey Complete - Thank you!"
title_image: 2024-06-05-Cloud-Video.jpg
date: 2025-02-10
authors:
   - holz
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Helmholtz Cloud
  - Services
excerpt: >
    Thank your for taking part in our survey on potential future Helmholtz Cloud services! With 1648 answers received we have a great basis for evaluation and, in consequence, improving the Helmholtz Cloud to best fit your needs!
---

# Survey on potential future Helmholtz Cloud services completed

The [Helmholtz Cloud](https://helmholtz.cloud/services/) offers a variety of digital services that are essential for scientists in their daily work - but yet some are still missing. We continuously collect ideas for future services but lacked their priority from a user's perspective - so we set up a short survey in which users could give us feedback on a bunch of service ideas we already collected. Additionally, users could tell us which services they are missing in Helmholtz Cloud. To round it up, we asked some general questions about HIFIS/Helmholtz Cloud.

The survey started on December 2<sup>nd</sup>, 2024 and ended roughly two months later on February 7<sup>th</sup>, 2025.

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right small nonuniform"
    alt="Figure points to diagrams an sustainability "
    src="{% link assets/img/posts/2024-07-31-cloud-quality-areas/HIFIS_Sustainability.svg %}"
    />
  </div>
</div>

## First insights

We are happy to already report some rough numbers, starting with a great participation of 1,648 persons all over Helmholtz.
Some of the service ideas were confirmed to be very interesting for users (such as a Project Management Tool, Overleaf PRO, Visual Collaboration / Mindmapping Tool), whereas other service ideas show a quite balanced result between users being interested and users not being interested (Federated Messenger, Wiki, Video Conferencing Tool). Surprisingly, the Electronic Laboratory Notebook, which we thought would be very interesting for users, does not seem to be much demanded.

The participation among centers varied considerably: 12 of 18 centers provided answers, with numbers ranging from about 12 to about 300 answers per center. The distribution among research fields is quite balanced, with a little more participation from Health and a little less from Information and Earth & Environment.

Based on the response, it appears that the familiarity with HIFIS and Helmholtz Cloud is balanced among the participants. A considerable number of participants are either not familiar with us or have heard of us but do not have a clear understanding of what our Services are about.
Those participants who know about HIFIS or Helmholtz Cloud mostly heard about us from (Helmholtz) colleagues. The most frequently named Helmholtz Cloud services in use are Mattermost, Nextcloud (DESY Sync&Share, nubes and bwSync&Share) and Helmholtz Codebase.
From the survey participants perspective it would help to raise awareness if HIFIS and Helmholtz Cloud would be promoted more within the centers (e.g. top down). Additionally, lectures/workshops and information on the intranet would be appreciated.

## Stay tuned: More results coming!

Of course, these are only first rough numbers derived from the survey. Within the next weeks, we are going to have a closer look at the results, also including the correlation between answers. We will keep you updated!

At this point we would like to thank all participants of the survey for taking your time and giving us important insights. We can only improve our Service Portfolio (and broader: the awareness about HIFIS and Helmholtz Cloud) with your feedback and are happy that so many of you are interested in our work and appreciate Helmholtz Cloud.


## Questions or proposals?

Contact our Service Portfolio Manager at <support@hifis.net>
