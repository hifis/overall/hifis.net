---
title: "Helmholtz ID Simplifies Secure Login"
title_image: sign-pen-business-document-48148.jpg
data: 2025-01-09
authors:
 - jandt
 - "Apweiler, Sander"
layout: blogpost
categories:
 - News
tags:
 - Helmholtz ID
 - MFA
excerpt: >
   Helmholtz ID is advancing its mission to provide a user-friendly and highly secure authentication and authorization via multi-factor authentication (MFA). If you've already used a second factor to log in to your home organization's account and they share this information with Helmholtz ID, you won't be asked to provide another second factor.
---

# The Whims and Woes of Secure Login

Some of you may have already witnessed the need of duplicated multi-factor logins when using Helmholtz ID:

Since mid 2023, a secure multi factor-backed login (2FA/MFA) [became mandatory](https://hifis.net/doc/helmholtz-aai/howto-mfa/) when using Helmholtz ID for group management. On top, a user could also explicitly activate MFA for all other purposes. However, Helmholtz ID has so far not been able to honor if such a second factor has already been checked one step before when you logged in. 

The result: Double work for you, i.e. you needed to provide yet another second factor for login. The fun was limited, we know. Now we are able to simplify the process.

## Honoring of MFA During Login

If you already provided a second factor during login at your home organisation's account **and** your organisation transmits that information to Helmholtz ID, **you will not be prompted yet another time to provide a second factor**. This works already seamlessly for several Helmholtz Centers. 

With that, Helmholtz ID moves forward in its mission to deliver an authentication and authorization infrastructure (AAI) that is both user-friendly and highly secure.

## Still Getting Bothered by Repeated MFA Requests from Helmholtz ID?

There are four possible reasons:

- **Check the header and URL of the query.** Often, not Helmholtz ID is asking you for the second factor, instead a page from your own organisation.
- **You have not (yet) provided a second factor during login at your home organisation in the first place?** Then, Helmholtz ID will continue to ask for it, at least for the group management or if you decided to use it in past. That's how it should work. You might consider to activate MFA in your home organisation, that's safer anyway.
- **You have already provided a second factor and still get asked again by Helmholtz ID?** Then, most likely, your home organisation does not transfer the corresponding information correctly: 
    - Ask your IT department to implement the corresponding transfer, as [documented here](https://hifis.net/doc/helmholtz-aai/howto-mfa-transfer/).
    - You can also contact us via <support@hifis.net> if problems persist.
- **You were not asked by your home organisation but by Helmholtz ID to provide a second factor, although you have configured a second factor at your home organisation.** Some home organisation may only ask for a second factor if the connected service (Helmholtz ID) requested authentication, using a second factor. Sadly we are not yet able to send this request, but we are working on it.

## Contact

Should you have any questions or comments, don't hesitate to contact the Helmholtz Cloud team at <support@hifis.net>.
