---
title: "Code for Thought podcast on TEACH"
title_image: matt-botsford-OKLqGsCT8qs-unsplash.jpg
date: 2025-03-04
authors:
 - servan
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Media
  - Conference
  - Education
  - Incubator
  - Interview
excerpt: >
  Code for Thought just issued an episode on the 4th TEACH conference featuring Fredo Erxleben from HIFIS Software.
---

<div class="floating-boxes">
    <div class="image-box align-right">
        <a href="https://codeforthought.buzzsprout.com/1326658/episodes/16652890-de-teach-konferenz-in-berlin-im-november-2024-ein-ruckblick">
            <img class="right small nonuniform"
            alt="Banner image for the podcast"
            src="{% link assets/img/third-party-logos/code4thought.jpeg %}"/>
        </a>
    </div>
</div>

# An ear to the fourth TEACH conference in Berlin
In November 2024, Peter Schmidt was invited to speak at the [4th TEACH conference](https://events.hifis.net/e/teach4), held at the HiDA-hub in Berlin.
Peter is the host of [Code for Thought](https://codeforthought.buzzsprout.com/1326658), the podcast about software for research and the people who make it, and so he took the opportunity to interview the organisers and some of the speakers while he was there.

In [the resulting episode](https://codeforthought.buzzsprout.com/1326658/episodes/16652890-de-teach-konferenz-in-berlin-im-november-2024-ein-ruckblick), _recorded in German_, you can get different perspectives on a broad selection of topics on RSE education.
For example, Fredo Erxleben (HIFIS) tells the story of TEACH: why it came to be and how it is evolving.
You will also hear from speakers sharing their experiences with hybrid lectures, strategies for dealing with no-shows, and the importance of entanglement in moving information from short-term to long-term memory!
Plus, Anna-Lisa Döring (HIDA) gives her thoughts on why teaching skills often get overlooked in researcher evaluations - and why that's a shame.
Finally, listeners get a glimpse of what's in store for the next edition of the TEACH conference.

Happy listening!


## Get in Contact
* With _Code for Thought_: [Peter Schmidt](mailto:code4thought@proton.me)  
* With HIFIS: [HIFIS Support](mailto:support@hifis.net)
* With HIDA: [HIDA Course Team](mailto: hida-courses@helmholtz.de)
