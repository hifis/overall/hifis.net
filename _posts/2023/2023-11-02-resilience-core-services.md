---
title: "Resilience of HIFIS Core Services"
title_image: omar-flores-MOO6k3RaiwE-unsplash.jpg
data: 2023-11-02
authors:
 - spicker
 - jandt
 - huste
 - servan
layout: blogpost
categories:
 - News
tags:
 - Helmholtz Cloud
 - Helmholtz ID
 - Resilience
 - Codebase
 - Sustainability
excerpt: >
   HIFIS ensures continuous operations of core services by replicating them across multiple centers, providing inherent resilience in a federated environment.
---

{:.treat-as-figure}
{:.float-right}
![illustration]({% link /assets/img/illustrations/undraw_air_support_re_nybl.svg %})

# HIFIS Resilience: protecting core services in a federated environment

A federated environment, such as the one established through HIFIS, provides the key advantage of inherent resilience that comes from having multiple centres and resources working together. When one centre is impacted by an issue, whether it be a cyber attack or other unexpected events, like power shortages, HIFIS can rapidly activate a duplicate of its core service at an alternative location. This minimises downtime and ensures the continuous operation of the HIFIS services for our users.

Over the past three months, we have identified and prioritised the HIFIS core services. Since the end of September, the most important core services are replicated daily at other Helmholtz centers. Having started with ad hoc solutions, we will continue to improve the technical implementation and organisational structure to support the overall resilience of HIFIS in the long-term.

This commitment to resilience and adaptability ensures the continued availability of services like the Helmholtz ID or the HIFIS Helpdesk, even in the face of unexpected events.

## Questions? Comments? Proposals?
If you have anything to tell us, feel free to [contact us](mailto:support@hifis.net).