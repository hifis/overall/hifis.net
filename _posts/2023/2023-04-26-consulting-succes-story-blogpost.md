---
title: A Consulting Success Story
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2023-04-26
authors:
  - foerster
  - ravindran
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Consulting
excerpt: >
    A short story about what HIFIS software consulting can help you with.
---


<!--=======================-->
<!--== DIV: Floating Box ==-->
<!--=======================-->
<div class="floating-boxes">

<!------------->
<!---- DIV ---->
<!------------->
<div class="image-box">
  <img class="right medium nonuniform"
         alt="Awesome List Logo"
         style="min-width: 150px; max-width: 150px;"
         src="{% link assets/img/posts/2023-03-31-success-story/consulting.svg %}">
</div>

<!------------->
<!---- DIV ---->
<!------------->
<div class="text-box" markdown="1">


<!---- Heading ---->
## Who is HIFIS Consulting?

If you are looking for help with your software projects, [HIFIS Consulting](https://hifis.net/consulting) can help you with any topic related to software development.
Since its inception, the HIFIS Consulting team has been defined from scratch and implemented its [semi-experimental workflow](https://hifis.net/consulting-handbook/) for the researchers in the scope of Helmholtz.
Around 70 groups / individual researchers have used our services yet, ranging from merely getting oral advice / reassurances to detailed and prolonged coding support. 
However, we believe that there are still a lot of research teams out there that could benefit from software consulting on topics ranging from software licensing, publishing, and code reviews to detailed tasks like code optimization.

In this blog post, we’ll delineate a typical consulting scenario and how the help / advice was delivered, leading to a fruitful situation for the researchers. 
It might help teams to understand what we do and how we could help them.

The following post is based on a real-life experience. 
However, the names are changed for obvious reasons.
</div>
</div>

<!--=======================-->
<!--=======================-->


<!---- Heading ---->
## Alex's Software Challenge
Alex is a geologist. 
Alex’s research entails crunching sensor data from ocean bottom nodes and visualizing it across 1000 timesteps.
The tools to accomplish the goals are inherited from a former college. 
Alex can access a high-performance computing facility using the provided Python routines. 
Still, Alex is not an inclined computer programmer and finds the code slow and it does not scale very well. 
A single time step file took around four hours to process. 
This means a lot of running time, even for a reasonably spaced paper deadline. 
Alex makes a consulting request searching for code optimization possibilities to speed up the process.


<!---- Heading ---->
## How did we help?
Once the consulting request was received, we scheduled a call and talked in person with Alex. 
This was to understand the problem precisely, along with the context. 
The problem posed may not always be exactly as requested in the [Helpdesk](https://support.hifis.net). 
Hence, contextual information, e.g., target domain knowledge, deadlines, level of programming competency of the client itself, etc., are helpful in finding pragmatic and sustainable solutions.


<!--=======================-->
<!--== DIV: Floating Box ==-->
<!--=======================-->
<div class="floating-boxes">

<!------------->
<!---- DIV ---->
<!------------->
<div class="image-box float-left" style="min-width: 420px; max-width: 420px;">
  <img class="right medium nonuniform"
         alt="Awesome List Logo"
         style="min-width: 410px; max-width: 410px;"
         src="{% link assets/img/posts/2023-03-31-success-story/undraw_open_source.svg %}">
</div>

<!------------->
<!---- DIV ---->
<!------------->
<div class="text-box" markdown="1">
After the first meeting, we found that the Python code had other problems as well, in addition to the optimization problem. 
The code was written in an outdated version of Python and didn’t have version control for sustainable development. 
Also, the code was not written following standard best practices. 
First, we introduced them to [codebase.helmholtz.cloud](https://codebase.helmholtz.cloud) and recommended the Gitlab courses from [HIFIS Education](https://events.hifis.net/category/4/).

Once the code got transformed into a version-controlled git repository, we did the code audit to include standard best practices like exception handling and logging. 
Then, we deep-dived into the code and refactored it to facilitate the much-needed parallelizing.

Then we introduced multiprocessing adapting to the high-performance computing facility at the client’s disposal.

We improved the readability and sustainability of the code and significantly reduced the time and effort to analyze the sensor data. 
Any corruption in the 5&nbsp;million data points like NaN or any issues in processing causing, e.g., division-by-zero exceptions, was caught and logged for later analysis.

Finally, after discussing the publication intentions with the client, an appropriate license was chosen for the code project. 
It was decided to use the permissive MIT license for a more flexible approach for later use.

## Get in contact
If you want to start a new consultation, fill out the [short questionnaire](https://survey.dlr-pt.de/index.php?r=survey/index&sid=958212&lang=en&newtest=Y).

If you have comments, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>

</div>


</div>

<!-- Links -->

