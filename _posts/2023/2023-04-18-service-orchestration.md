---
title: "Cloud Service Orchestration for Helmholtz Imaging"
title_image: orchestration_components.png
data: 2023-04-18
authors:
  - jandt
  - klaffki
  - klotz
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - Use-Case
tags:
  - Health
  - Helmholtz Imaging
  - Service Orchestration
  - dCache
  - Codebase
  - GitLab
  - openstack
  - OIDC Client
  - Helmholtz AAI
excerpt: >
  Interconnecting cloud services &mdash; sometimes dubbed Service Orchestration &mdash; allows to automate complex workflows,
  a precondition to ultimately set up reliable, scalable, reproducible and adaptable data processing pipelines for science.
  A typical example is to automatically and continuously fetch, move, process, publish and store scientific data, possibly in multiple iterations and for different audiences.
  We demonstrate our first pilot orchestration of an imaging use case in collaboration with Helmholtz Imaging.

---

# Automated, distributed image processing pipelines

## From Cloud Services to Service Pipelines

In its first years, HIFIS has set up the Helmholtz Cloud to interconnect and broker services from Helmholtz for Helmholtz.
The [Helmholtz Cloud Portal](https://helmholtz.cloud/) is the central point where users have access to all services and combine them to suit their work flow.

Interconnecting cloud services &mdash; sometimes dubbed Service Orchestration &mdash; allows to automate complex workflows,
a precondition to ultimately set up reliable, scalable, reproducible and adaptable data processing pipelines for science.
A typical example is to automatically and continuously fetch, move, process, publish and store scientific data, possibly in multiple iterations and for different audiences.

Such interconnection of services and resources can in principle be implemented by end users,
thanks to the open interfaces of most offered cloud services and the service-agnostic authentication and authorisation infrastructure (AAI) with its single-sign on functionality.

**However:** This is the point where extensive knowledge on the underlying technologies is necessary in order to implement complex service orchestration workflows.

Whenever you plan to implement such complex use cases yourself: Yay!! Feel free to [contact us](mailto:support@hifis.net) &mdash; We are always happy to hear from you and even happier to assist!

Mostly, though, you will probably be **glad to use (and adopt) pre-confectioned pipelines**.
So, let's see what's already possible!

<div class="floating-boxes">
<div class="image-box align-right">
<img class="right small nonuniform"
alt="map of Germany showing the five Helmholtz Centres involved and their service: DESY / dCache, HZB / Helmholtz AAI, Jülich / openstack, HZDR / GitLab, KIT / OICD Client"
src="{% link assets/img/posts/2023-04-18-service-orchestration/map_services_HI.svg %}"
/>
</div>
</div>

## A Pilot Use Case from Helmholtz Imaging

[Helmholtz Imaging](https://helmholtz-imaging.de/), a ''sister'' platform to HIFIS, aims to improve access for scientists on innovative scientific imaging modalities, methodological richness, and data treasures within the Helmholtz Association.
HIFIS can support this mission through seamless interconnection of the cutting-edge IT infrastructures in Helmholtz as orchestrated services.

Our pilot implementation of such orchestration showcases the interconnectivity of **five different Helmholtz centres**:
Cloud services are located in three centres,
an interconnecting component is developed by a fourth,
and the scientific user comes from a fifth centre.

The pilot also demonstrates the **modularity** of such pipelines,
allowing to exchange or scale up parts of the pipeline, whenever beneficial.

The **data we use to showcase our pipeline** originates from a
[research paper published in 2020 [1]](https://doi.org/10.1083%2Fjcb.202010039).
The authors looked into the role of intracellular organelles, such as microtubules, for insulin transportation and secretion.
They segmented organelles from high-resolution tomographic images, and provided mesh generation and rendering of 3D organelle reconstructions in primary mouse betacells as a model system.

<div class="floating-boxes">
<div class="image-box align-left">
<img class="left medium nonuniform"
alt="five Cloud symbols, showcasing the logical workflow"
src="{% link assets/img/posts/2023-04-18-service-orchestration/orchestration_components_2.svg %}"
/>
</div>
</div>

### Workflow in a nutshell

Using this exemplary data, we show how the data is handled and processed, starting from raw images until ''publication''.
For more details, you may also check our [presentation on an earlier version of this demonstrator <i class="fas fa-external-link-alt"></i>](https://nubes.helmholtz-berlin.de/s/n7LYPMTTydT3TEE/download?path=%2F&files=CloudDemo_AS_SLIDES.pptx).


- The primary data of the researcher (_HZB_) was stored in a Sync&Share. We do not advise to do this, as there are more sophisticated data storage solutions such as [dCache InfiniteSpace](https://helmholtz.cloud/services/?filterSoftwareNames=dCache&serviceID=9b6c63a4-d26b-4ea6-b8b0-88c0be5ea610) (_DESY_). So, we transfer this first data to the dCache for all later usage and updates.
- The raw data, now located at _DESY_, is then transferred to a [virtual machine](https://helmholtz.cloud/services/?filterSoftwareNames=OpenStack&serviceDetails=svc-36fef321-6a91-4dc0-aff8-3fd4ebc6008c) located in _Jülich_, to do the processing there, with intermediate and final results sync'ed back to dCache at _DESY_, and prepared for publication on an open website built and hosted at _HZDR_.
- **Almost the complete workflow is automated.** The script magic is supplied via [Helmholtz Codebase (Gitlab)](https://helmholtz.cloud/services/?filterSoftwareNames=GitLab&serviceDetails=svc-c5d1516e-ffd2-42ae-b777-e891673fcb32) at _HZDR_, allowing for collaborative, versioned development of scripts and further usage-specific magic.
- Minimal user interaction is necessary in the beginning for the primary login via AAI; no user interaction at all for subsequent iterations / updates of the data.
- The secure transfer of user login information between the services, without further manual interaction by the user, is mediated by a technology calls [OIDC Agent](https://hifis.net/doc/helmholtz-aai/howto-users/#how-to-interact-with-unity-via-oidc-agent), provided by _KIT_.

<div>
<img
alt="Workflow, from raw scripts and data over intermediate segmentation results, to published results"
src="{% link assets/img/posts/2023-04-18-service-orchestration/from_raw_data_to_result.svg %}"
/>
</div>

<div class="floating-boxes">
<div class="image-box align-right">
<img class="right medium nonuniform"
alt="Cartoon showing HIFIS staff juggling with Helmholtz Cloud services for scientific users"
src="{{ site.directory.images | relative_url }}/hifisfor/poster-2b.svg"
/>
</div>
</div>

### Why should I use this?

- All transfers and steps occur **within Helmholtz infrastructure**, with secure underlying authentication and authorisation methods. That is, all intermediate steps can be **finely controlled** to be private, group-shared and/or public, just as desired. Likewise, different steps of the pipeline can be set up to be accessible by different user (groups).
- The workflow can be set up to **persist** for a definable time frame, even when the original user(s) are not online anymore. For example, newly incoming raw data on dCache can trigger the pipeline to be **continuously updated** and provide correspondingly updated results.
- You need increased or reduced compute power at different points in time? You want to exchange the storage because the majority of primary data piles up somewhere else? You want to introduce another specific processing step employing another cloud service? No big deal: The **modularity of the whole set-up** allows to exchange or replace any part of the pipeline, whenever needed.

**Use and combine the capabilities of [Helmholtz Cloud](https://helmholtz.cloud) to your best, without needing to solely rely on local resources.**

### That sounds cool. But... how to set this up?

The confection of this kind of orchestrated IT services and pipelines for specific classes of use cases will be a **major focus of HIFIS** from now on.

If you have a corresponding use case — any sort of complexity is welcome — please [**get in touch with us**](mailto:support@hifis.net). We will be happy to assist and build cool stuff!

In the near future, generalized pipelines will be made available via our pages and the cloud portal, so: Stay tuned!

## Get in contact
For HIFIS: [HIFIS Support](mailto:support@hifis.net)
For Helmholtz Imaging: [Helmholtz Imaging Helpdesk](mailto:helpdesk@helmholtz-imaging.de)

---

## Literature

[Müller, A., et al. (2021). 3D FIB-SEM reconstruction of microtubule–organelle interaction in whole primary mouse β cells. J Cell Biol, 220(2)](https://doi.org/10.1083%2Fjcb.202010039).

---

## Changelog

- 2023-06-28 -- Added Link to Helmholtz Cloud portal entry for newly onboarded dCache service, adapted wording
