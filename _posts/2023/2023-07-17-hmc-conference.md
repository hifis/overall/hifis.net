---
title: "HMC Conference: Call for Abstracts is open"
title_image: photo-of-open-signage-2763246.jpg
data: 2023-07-17
authors:
 - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - News
tags:
 - Event
 - Announcement
 - HMC
 - Incubator
excerpt: >
   The HMC conference 2023 wants to provide you with a platform to present your metadata project, to share ideas with the metadata community, to get an insight into recent developments and to network. it will take place 10.-12.10.2023.
---

<div class="image-box align-right">
<img class="right large nonuniform"
alt=""
src="{% link assets/img/posts/2023-07-18-HMC-conference/HMC-Conference_2023_Visual_Email.jpg  %}"
/>
</div>

# Better Research Through Better Metadata
## HMC, the Helmholtz Metadata Collaboration, invites you to their second conference

The HMC conference 2023 wants to provide you with a platform to present your metadata project, to share ideas with the metadata community, to get an insight into recent developments and to network.
The conferecne will focus on three main areas:
Assessing and monitoring the state of FAIR data, Facilitating connectivity of research data and Transforming (meta)data recommendations into implementations.
More details on those topics and their subtopics can be found [here](https://events.hifis.net/event/891/program).
The online event will happen on **10.-12.10.2023**.

## Registry and Call for Abstracts are open
Sounds interesting? Register [here](https://events.hifis.net/event/891/registrations/912/).
The [call for abstracts](https://events.hifis.net/event/891/abstracts/) for talks or posters is also open. The submissions must belong to one of the three tracks listed above and the deadline is **16.08.2023**.

## Get in Contact
For the HMC conference: [HMC](mailto:event@helmholtz-metadaten.de)