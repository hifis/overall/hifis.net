---
title: Last Minute! Open Seats in Course "First Steps in Python"
title_image: adi-goldstein-mDinBvq1Sfg-unsplash_shrinked.jpg
date: 2023-05-19
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Event
  - HIFIS Education
excerpt: >
    There are still open seats in the course "First Steps in Python" on 25 May!
---

# Open Seats left in "First Steps in Python"
The online course "First Steps in Python" offered by HIFIS Education takes place on **Thursday 25th of May** and aims at beginners.
You are only expected to install the software on your computer as a prerequisite, details can be found [here](https://events.hifis.net/event/851/).

This workshop is conceptualized as a one-day event that covers an introduction to the Python programming language, its basic language and programming concepts. 
It leaves you with fundamental knowledge as a jumping-off point for self guided learning.

## Registration is still open!
The registration for this course is open until **Tuesday 23rd of May**, so do not hesitate to [secure your place](https://events.hifis.net/event/851/registrations/860/)! 

## Questions? Comments? Proposals?
If you have anything to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
