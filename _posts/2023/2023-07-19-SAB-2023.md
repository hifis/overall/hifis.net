---
title: "HIFIS Scientific Advisory Board 2023"
title_image: SAB_1_notext2.png
date: 2023-07-19
authors:
 - spicker
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Report
  - Event
excerpt: >
  The Scientific Advisory Board 2023 provided again valuable inspiration for the further development of HIFIS.
---

<div class="floating-boxes">
<div class="image-box align-right">

<img class="right medium nonuniform"
alt="Screenshot of Online Meeting"
src="{% link assets/img/posts/2023-07-19-SAB-2023/SAB_3.jpg %}"
/>
</div>
</div>


<div style="clear: left"/>

# International experts discussed further development
On 13<sup>th</sup> July 2023, the [HIFIS Scientific Advisory Board](https://hifis.net/structure/members-sab.html) met online to discuss the progress of HIFIS over the last year and to provide advice for future development. 
Ten international IT experts participated and, together with our HIFIS management, discussed upcoming opportunities and challenges. 
Both reliable operation and user-oriented further development now need careful management. 
With the contributions of the advisory board, this evolution takes place well embedded in the global scientific context without neglecting Helmholtz requirements. 

## Discussion how HIFIS can improve the following task at Helmholtz
Within HIFIS, we identified four tasks that we need to focus on, so we presented them to the advisory board.
Their external contributions were particularly desirable on these topics and were discussed in four breakout rooms: 
* Contribution that HIFIS can make to enhance operational cybersecurity in the Helmholtz Centres.
* Boosting of Research Software Development. 
* Further development of raw scientific software and services into sustainable offerings e.g. by funding projects.
* Improving of the scientific Helmholtz Cloud portfolio.

## Comments and suggestions
Contact us anytime in English or German / Deutsch: [contact us](mailto:support@hifis.net)!


