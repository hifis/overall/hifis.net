---
title: "Helmholtz Research Software Directory updated"
title_image: pexels-cottonbro-5483071.jpg
date: 2023-11-09
authors:
 - meessen
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Community
  - Cloud Services
excerpt: >
  The Helmholtz Research Software Directory received a major update.
---

# New major update for the Helmholtz RSD

The Helmholtz Research Software Directory ([https://helmholtz.software](https://helmholtz.software)) was updated to version [v2.0.0](https://github.com/research-software-directory/RSD-as-a-service/releases/tag/v2.0.0) of the [Research Software Directory](https://github.com/research-software-directory/RSD-as-a-service).

The new release is focused on better usability and visual appearance of the RSD. It specifically includes a new layout for the software, project and organisation pages.

<center>
<img src="{% link /assets/img/posts/2023-11-09-RSD-update/screenshot.png %}" alt="Screenshot of Helmholtz RSD Software overview">
</center>

The new release also includes enhanced filtering options for software entries, e.g. by assigned keywords or programming language. Further options are currently being developed, in particular dedicated filtering by Helmholtz Research Fields and PoF topics.

Upcoming updates will include more enhancements such as better handling of logos and images of software entries, a feature that automatically collects citation information of a software entry based on DOIs, and personal RSE pages. 

Stay tuned for more updates and add your software to the RSD:

<center>
<a alt="Link to the HIFIS RSD" href="https://helmholtz.software" class="btn btn-primary btn-lg m-1" target="_blank">https://helmholtz.software</a>
</center>
