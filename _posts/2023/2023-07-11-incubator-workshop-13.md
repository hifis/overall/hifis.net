---
title: "Helmholtz Incubator Workshop 2023 in Berlin"
title_image: 20230707_Incubator_3.jpg
date: 2023-07-11
authors:
 - spicker
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
  - Report
  - Event
tags:
  - Helmholtz Incubator
excerpt: >
  With the 13<sup>th</sup> Incubator Workshop, all platforms (HIFIS, Helmholtz AI, Helmholtz Imaging, HIDA, HMC) continued to exploit their high creative potential to implement far-reaching benefits for the scientific communities. All experts emphasized the enormous value that the Incubator platforms have for the Helmholtz Association.
---

<div class="floating-boxes">
<div class="image-box align-right">

<img class="right medium nonuniform"
alt="Helmholtz President Otmar D. Wiestler giving a speech"
src="{% link assets/img/posts/2023-07-11-incubator-workshop-13/20230707_Incubator_4.jpg %}"
/>
</div>
</div>


<div style="clear: left"/>

# Helmholtz Incubator Workshop 2023 in Berlin
With the 13<sup>th</sup> Incubator Workshop, all platforms (HIFIS, Helmholtz AI, Helmholtz Imaging, HIDA, HMC) continued to exploit their high creative potential to implement far-reaching benefits for the scientific communities.

## A unique initiative in the scientific landscape
Both the Helmholtz President Otmar D. Wiestler (picture on the right) and the invited external experts emphasized the enormous value that all Incubator platforms have for the Helmholtz Association. The initiative is regarded as unique in the scientific landscape in bringing together digital expertise across a broad spectrum of science. Since 2017, the Incubator and its experts "made invaluable contributions to a sustainable, long-term and association-wide structure that mobilizes significant synergies and generates substantial benefit for Helmholtz and its research programs", Wiestler stated. He and the experts considered it important for the future that "the momentum and the dynamic spirit in all platforms" is maintained. "To open the Incubator Framework to a growing information & data science community in order to systematically exploit the Incubator as a vivid exchange platform" will be one of the future challenges.

## Excellent ratings for all platforms 
2022 and 2023, all platforms and also the schools, concluded their respective evaluation with the rating *highly recommended to continue*.
During this process, all platforms have developed strategies for their future development and enhancements. Numerous supporting recommendations from the evaluation panels need to be implemented. Following the advice to intensify cross-platform collaboration, these valuable ideas were brought together during the workshop. In several breakout sessions, the participating experts from all research fields generated comprehensive ideas, ranging from short-term plans to far-reaching missions.

In fall 2023 and based on the fruitful and valuable results of this workshop, the Helmholtz Assembly of Members will decide on the future of the Incubator and the platforms, with the upcoming Incubator Workshop fleshing out the details on their continuation and development.

## Get in Contact

[HIFIS Support](mailto:support@hifis.net)