---
title: "New functionality in Plony: Change Requests"
title_image: background-cloud.jpg
date: 2023-05-30
authors:
 - holz
layout: blogpost
categories:
  - News
tags:
  - Cloud Services
excerpt: >
  With the Update of Plony, Service Providers can edit Service Information in Plony
---

## Edit your service information directly in Plony!

With the introduction of the “Online Form” any member of the Service Operation Group can suggest changes for one or multiple service information. For example: The nubes Service Operation Group can change the content of any field of the nubes Service Description.

## Roles in the Change Request process

The roles included in the Change Request process are the Service Operation Group, the Object Owner and the Service Portfolio Manager:
![Alt-Text]({{ site.directory.images | relative_url }}posts/2023-05-30-new-functionalities-in-plony/Roles_in_Change_Request_Workflow.png)

- The Service Operation Group includes one or more persons operating a service (usually admins) and therefore knowing the service best. Whenever service information is outdated, a member of the Service Operation Group can update the information and thereby create a change request.
- The Object Owner can review/edit change requests from the Service Operation Group and forward the Change Request for approval to HIFIS. Additionally, the Object Owner manages the members of the Service Operation Group by adding/deleting members. The Object Owner is automatically filled with the person named as Service Manager during the Onboarding Process; this entry can be changed so that Object Owner and Service Manager are different persons.
- The Service Portfolio Manager receives Change Requests, may discuss them with the HIFIS Team and decides about acceptance/denial of the suggested changes.

You find a detailed description of how changing service information works in chapter V of the [Plony instruction](https://plony.helmholtz.cloud/static_files/documents/instruction_service_portfolio.pdf/@@download/file).

## Service Providers: We need your support!

If you already provide a service to Helmholtz Cloud: Please log in to [Plony](https://plony.helmholtz.cloud/) and check the tab “Ownership” in your service – is the right person set as Object Owner? As Object Owner, please add the correct members to your Service Operation Group. If you miss someone in the list of possible Service Operation Group members, please ask them to initially login to Plony via Helmholtz AAI. They will then be added to the list of possible Service Operation Group members.