---
title: "The 6th issue of our newsletter is here"
title_image: paula-hayes-Eeee5H-yuoc-unsplash.jpg
date: 2023-11-20
authors:
  - servan
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Newsletter
excerpt: >
  Subscribe to our newsletter to get all further issues directly delivered to your inbox!
---

# The Latest News from HIFIS

<div class="floating-boxes">
  <div class="image-box align-right">
    <img  class="right small linear"
          alt="mailbox"
          src="{% link assets/img/illustrations/undraw_mailbox_re_dvds.svg %}"/>
  </div>
</div>

Sustained funding for HIFIS and the other platforms, new communities using HIFIS services, a new version of the research software directory &mdash; now listing nearly 200 software, some news to read and listen to about the software award, and finally our journey towards a better resilience: __find out the most recent news from HIFIS in the latest edition of our newsletter [here](https://hifis.net/newsletter/2023-11/2023-11-HIFIS-Newsletter.html)__ (or in [PDF](https://hifis.net/newsletter/2023-11/2023-11-HIFIS-Newsletter.pdf)).

To receive further issues or spread the word, [here is where to subscribe](https://lists.desy.de/sympa/subscribe/hifis-newsletter).
Check also our [previous newsletters]({% link newsletter/index.md %}) and if you don't want to wait for the next edition for more news, you can regularly check our [blog posts]({% link posts/index.html %}).

## <i class= "fas fa-address-book"></i> Get in contact
Feel free to write to us at [HIFIS Support](mailto:support@hifis.net) with any question or suggestion.