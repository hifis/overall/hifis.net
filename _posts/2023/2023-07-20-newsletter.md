---
title: "The July Issue of our Newsletter is here"
title_image: paula-hayes-Eeee5H-yuoc-unsplash.jpg
date: 2023-07-20
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Newsletter
excerpt: >
  Subscribe to our newsletter to get all further issues directly delivered to your inbox!
---

<div class="floating-boxes">
<div class="image-box align-right">

<img class="right medium nonuniform"
alt=""
src="{% link assets/img/posts/2023-07-20-newsletter-july/mathyas-kurmann-fb7yNPbT0l8-unsplash_small.jpg %}"
/>
</div>
</div>

# The Latest News from HIFIS
While this newsletter might find some of its readers at beaches or mountainsides, here at HIFIS we're still very busy. 
The latest issue of our newsletter just got sent to all subscribers.
In it, we bundled up the latest news from HIFIS and the Helmholtz Incubator:

As you might have seen, [Helmholtz Cloud has got a facelift](https://hifis.net/news/2023/07/19/portal-relaunch.html). 
Also, we expanded the service portfolio further, as now also [large-scale storage is available](https://www.hifis.net/news/2023/07/11/dcache-in-helmholtz-cloud.html) for research and service pipelines.

We were also active in the Incubator context with the 13<sup>th</sup> [Helmholtz Incubator Workshop](https://www.hifis.net/news/report/event/2023/07/11/incubator-workshop-13.html).
Especially if you write Research Software in Helmholtz, you should consider the [first Helmholtz Incubator Software Award](https://www.hifis.net/news/2023/07/03/software-award.html). 

Upcoming events to keep in mind are the [Teach 3 Conference](https://www.hifis.net/news/2023/07/03/teach3-call.html), [Incubator Summer Academy](https://www.hifis.net/news/2023/06/20/summer-academy.html) and, arriving after editorial deadline, the [HMC Conference](https://www.hifis.net/news/2023/07/17/hmc-conference.html).
And, last but not least, if you are interested in the educational activities from HIFIS, tune in to the German episode of [Code for thought](https://www.hifis.net/news/2023/06/29/podcast-education.html).

## Interested in Our Next Steps?
Then don't miss our next newsletters to track the progress and perspectives of HIFIS!

**[Subscribe here](https://lists.desy.de/sympa/subscribe/hifis-newsletter)**, and spread the word and link.
Check also our newsletters [archive]({% link newsletter/index.md %}) and [blog posts]({% link posts/index.html %}).

## Questions? Comments? Suggestions?
Don't hesitate to contact the [HIFIS Support](mailto:support@hifis.net)!