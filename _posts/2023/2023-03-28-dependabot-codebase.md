---
title: "Helmholtz Codebase: Automating Dependency Updates"
title_image: clark-tibbs-oqStl2L5oxI-unsplash.jpg
date: 2023-03-28
authors:
  - hueser
  - huste
  - ziegner
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - GitLab
  - Codebase
  - Dependabot
excerpt: 
      We are happy to announce the general availability of
      <a href="https://dependabot-gitlab.gitlab.io/dependabot/">automated dependency update management</a>
      in the <a href="https://codebase.helmholtz.cloud/">Helmholtz Codebase GitLab</a>.
      It provides automatic dependency updates in order to
      keep your software up-to-date and secure.

---

{{ page.excerpt }}

## Why should you bother?

Outdated dependencies with known security flaws is one of the most
frequent security issues that get exploited most often.
Enabling automated dependency updates helps you saving time
by keeping track of all dependency updates, automating
time-consuming recurring dependency update tasks, and staying
secure in your application.

## All Information in One Place

Beside the versions of the old and new dependency, it provides you with 
information about the release notes as well as the commit history. 

## How does it work?

The bot will automatically create Merge Requests for dependency updates:

!["GitLab Merge Request created by HIFIS Bot"]({% link assets/img/posts/2023-03-20-dependabot-codebase/dependabot_merge_request.png %})

The list of supported ecosystems is given [here](https://docs.github.com/en/code-security/dependabot/dependabot-version-updates/about-dependabot-version-updates#supported-repositories-and-ecosystems).
Among others, Python, Docker or Git submodules are supported. If you want to
use Dependabot for your software project, you can find the setup instructions
in our [documentation](https://hifis.net/doc/software/gitlab/dependabot/#configuration).

## Comments and Suggestions

If you have suggestions, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>

## References

* [Documentation](https://hifis.net/doc/software/gitlab/dependabot/)
