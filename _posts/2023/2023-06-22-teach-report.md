---
title: "Recap and Report on the TEACH Conference 2022"
title_image: glenn-carstens-peters-RLw-UC03Gwc-unsplash.jpg
date: 2023-06-21
authors:
 - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - HIFIS Education
  - Report
  - Event
excerpt: >
  
---

<div class="floating-boxes">
<div class="image-box align-right">
<img class="right medium nonuniform"
alt="Banner image for the TEACH II"
src="{% link assets/img/posts/2022-10-27-teach2/HiDA_Visual_TEACH2_Visuals_16-9.jpeg %}"
/>
</div>
</div>


# The second TEACH Conference, Open Up! 2022
## A short recap

The conference *TEACH – Talk about Education Across Communities in Helmholtz* was initiated in 2021 by educators and trainers from the Helmholtz Incubator platforms.
This event was repeated in November 2022, this time under the motto *Open Up!*. 
The contributions focused on the topic of open educational resources (OER) as part of the Open Science movement. 
Ten talks and workshops were offered by 14 contributors in parallel sessions.
Posters presenting the Helmholtz platforms were set up in a gallery, like a traditional poster session at a in-person conference.
If you are interested in the contents, have a look at the [Book of Abstracts](https://events.hifis.net/event/312/book-of-abstracts.pdf) and the [slides](https://events.hifis.net/event/312/timetable/) beneath the respective slot in the timetable.

## The Report
After the event, the organizers took some time to reflect on the outcome and prepared a concise [report](https://zenodo.org/record/7956292) on the conference.
It was published on [Zenodo](https://zenodo.org/), the open repository operated by [CERN](https://home.cern/). 
With the publication on Zenodo, the report got persistantly citeable with a DOI (Digital Object Identifier) and was added to the [HIFIS Community on Zenodo](https://zenodo.org/communities/hifis/?page=1&size=20).
You can also find the [report of the first conference](https://zenodo.org/record/7956355) on Zenodo.

## Core Findings
"Refering to the takeaway messages from the last TEACH conference, there is still a great demand for improving the communication within the Helmholtz Society. 
Events like the TEACH conference can bring people together. 
But as developments in open educational resources point out there is also a demand for platforms, tools and workflows for collaboration. 
One example for this is the website [Workshop Materials]({% link services/software/training.md %}#workshop-materials) by HIFIS.
It can be seen as an initial step in this direction. 
The goal is to create many points of contact in the Helmholtz Society to establish a culture of sharing and networking. 
A new focus at the second TEACH conference was placed on mental health this year. 
Often neglected in the past, its importance and an awareness thereof increased during the COVID-19 pandemic. 
This demand was recognized and addressed in the present teaching community. 
Here in particular an exchange between trainers among the Helmholtz Centres is necessary to develop concepts and implement further courses." [From the Report on the TEACH Conference 2022, p. 2.]
 
## Get in Contact
For HIFIS Education: [HIFIS Support](mailto:support@hifis.net)

---

## References
* Schmahl, Ines, Schworm, Stephanie, Erxleben, Fredo, & Lemster, Christine. (2023). Report on the TEACH Conference 2022 (Revision 2023-05-16). Zenodo. <a href="https://doi.org/10.5281/zenodo.7956292">https://doi.org/10.5281/zenodo.7956292</a>
* Erxleben, Fredo, Ehrmanntraut, Sophie, Ferguson, Lea Maria, & Florian, Mona. (2023). Report on the TEACH Conference 2021 (Revision 2023-05-16). Zenodo. <a href="https://doi.org/10.5281/zenodo.7956355">https://doi.org/10.5281/zenodo.7956355</a>
