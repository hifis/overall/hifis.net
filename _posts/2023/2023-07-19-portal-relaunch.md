---
title: "New Helmholtz Cloud Landing Page"
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
data: 2023-07-19
authors:
 - "Wagner, Sebastian"
 - "Beermann, Thomas"
 - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - News
tags:
 - Helmholtz Cloud
 - Announcement
excerpt: >
   We’re excited to finally show you what has been in the works for the past few months: a completely redesigned landing page for the Helmholtz Cloud Portal along with a design refresh!
---

<div class="floating-boxes">
<div class="image-box align-right">
<a href="https://helmholtz.cloud/">
<img class="right large nonuniform"
alt="Screenshot of Helmholtz Cloud's landing page"
src="{% link assets/img/posts/2023-07-18-portal-relaunch/Screenshot_Cloud.png %}"
/>
</a>
</div>
</div>
<div class="text-box" markdown="1"> 

# Helmholtz Cloud Design Update

We’re excited to finally show you what has been in the works for the past few months: 
a completely redesigned [landing page](https://helmholtz.cloud/) for the Helmholtz Cloud Portal along with a design refresh!
Things are moving fast at HIFIS and we felt the original landing page needed a few updates to reflect the expanded service portfolio.

## Putting Scientists at the Center
The landing page is the first point of contact when using Helmholtz Cloud Services. 
For new users, getting a clear understanding of Helmholtz Cloud is vital. 
Therefore, all texts owere re-written to put scientists at the center, highlighting how they can use services to support their daily work.
Not only can scientists organize their team’s work but they can also assemble services into customized workflows, access some of the most powerful resources available at Helmholtz and collaborate with people around the world.

## The Cloud Built for Science
Continuing on the theme of putting scientists and their work at the center you may have noticed the video at the top of the landing page. 
It represents a real-world use case of Helmholtz Cloud services. 
As Helmholtz Cloud gained wider adoption, so grew the number of [use cases](https://hifis.net/usecases) covered on [HIFIS.net](https://hifis.net/). 
Their aim is to showcase a project or a workflow and let scientists discover ways others use Helmholtz Cloud with contact info readily available to share experience.
The video at the landing page, for example, links to a detailed description of the [DataHub of the Research Field "Earth and Environment"]({% link _posts/2022/2022-10-04-use-case-datahub.md %}). 

## We’re Just Getting Started
This design update marks the beginning of a series of updates coming to HIFIS websites and applications. 
Apart from the visual refresh it also lays the foundation for a unified user interface design system for HIFIS overall. 
Development is going to continue and you may see updates all over HIFIS pages, so stay tuned for more updates!

## Questions? Comments? Proposals?
If you have anything to tell us, don't hesitate to [contact us](mailto:support@hifis.net).