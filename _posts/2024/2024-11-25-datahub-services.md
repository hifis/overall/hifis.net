---
title: "DataHub Services Closely Linked With HIFIS"
title_image: 2024-06-05-Cloud-Video.jpg
date: 2024-11-25
authors:
  - Hammitzsch, Martin
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Cloud
  - Earth & Environment
  - Earth DataHub
excerpt: >
  A synergy, adding value for users and providers likewise.
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Screenshot of Earth DataHub website"
    src="{% link assets/img/posts/2024-11-25-datahub-services/earth_data.jpg %}"
    />
  </div>
</div>

# DataHub and Its Earth Data Portal
The DataHub is a cross-center initiative with participation of the seven research centers (AWI, FZJ, GEOMAR, GFZ, Hereon, KIT, UFZ) of the [Research Field Earth and Environment in the Helmholtz Association](https://www.helmholtz.de/en/research/research-fields/earth-and-environment/). As a federated, interoperable and open data infrastructure, a range of digital services facilitate research data sharing and collaboration between researchers from different scientific disciplines and institutions. The DataHub provides researchers with a digital ecosystem to store, manage and share their data. It also provides data analysis, data visualization, and data annotation tools to improve data processing and interpretation.

DataHub services continously become accessible through the [Earth Data Portal](https://earth-data.de/) as a single access point for successively delivering data, key analytics and visualizations, demonstrating the value of integrated Earth system data for knowledge generation in science and society.

# HIFIS Offerings for DataHub

Across all research fields, the [Helmholtz Incubator for Information and Data Science (HIDS)](https://www.helmholtz.de/en/research/challenges/information-data-science/) initiates collaborative development, solutions and implementation of the digitalization strategy, and thereby the DataHub of the Research Field Earth and Environment is linked to several Incubator initiatives such as the HIDS platform [HIFIS](https://hifis.net/).

The DataHub integrates seamlessly with HIFIS, which supports a FAIR and open digital infrastructure. DataHub services utilize HIFIS services like [Helmholtz ID](https://hifis.net/aai/) for authentication and authorization, the [Helmholtz Research Software Directory (RSD)](https://helmholtz.software/) for offering research tools, and the [Helmholtz Cloud](https://helmholtz.cloud) for opening the access to data infrastructure services.

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="left medium nonuniform"
    alt="Screenshot of Helmholtz Cloud Portal filtering DataHub"
    src="{% link assets/img/posts/2024-11-25-datahub-services/cloud-portal-datahub.png %}"
    />
  </div>
</div>


# Helmholtz Cloud, Support and Linked Services

The [Helmholtz Cloud](https://helmholtz.cloud) follows clearly defined processes to guarantee secure handling of services and supports service providers, such us DataHub, when offering broader access to their services via the Helmholtz Cloud Portal. Also the quality of operations is increased, as services are monitored thoroughly and structured. As part of this, service providers and Helmholtz Cloud portfolio managers interactively process service information and furnish it in the [Plony Database](https://plony.helmholtz.cloud/). All necessary information are available via an API.

Thus HIFIS supports in professionalising operation of scientific services and in opening access. In addition, information about the services is available for reuse in use cases levaraging open science. So registering the DataHub services centrally in the Helmholtz Cloud makes it easy to automatically retrieve and publish the [services on the Earth Data Portal](https://earth-data.de/tools?qf=genericType/service&offset=0&q=DataHub) and to make them findable and accessible.

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Screenshot of Earth DataHub Services "
    src="{% link assets/img/posts/2024-11-25-datahub-services/earth-data-services.png %}"
    />
  </div>
</div>


# DataHub Services

At present, eight [services of DataHub](https://helmholtz.cloud/services/?filterSearchInput=datahub&sortByAttribute=title) are offered in the Helmholtz Cloud Portal and thus likewise in the [Tools & Services section of the Earth Data Portal](https://earth-data.de/tools?qf=genericType/service&offset=0&q=DataHub):
* **Earthquake Explorer**
  The GFZ Earthquake Explorer is the next-generation mobile-aware earthquake information system. It augments the existing GEOFON data products which have provided rapid global earthquake information to users world-wide for many years.
* **GIPP Experiment Database**
  The Experiment Database of the Geophysical Instrument Pool Potsdam (GIPP) contains information about all supported experiments since 1993, including links to the collected data.
* **Gravity Information Service** (GravIS) 
  GFZ´s GravIS, developed and operated in collaboration with AWI and TU Dresden, visualizes and describes user-friendly mass transport products for dedicated geophysical applications derived from the gravimetric satellite missions GRACE and GRACE-FO.
* **International Centre for Global Earth Models** (ICGEM)
  A data base for global Earth gravity field models and calculation service for gravity related functionals. 
* **O2A Registry** (Ocean and Climate Sensor Management)
  Manage metadata for platforms, devices and sensors or product related information for permanent transparency.
* **Sensor Management System**
  Manage metadata for devices, platforms & measurement configurations.
* **Timeseries Management**
  The Timeseries Management allows to store, visualise, organise and share timeseries data and provides access to a (broad) selection of data from different long-term projects and campaigns (e.g. TERENO, MOSES).

Further DataHub services will be rolled out successively for broader adoption over the course of the next months in 2025. Beyond this, also services provided by other Helmholtz centres and initiatives complement the service portfolio of DataHub and generate true synergies. Check all currently available 42 services on [Helmholtz Cloud](https://helmholtz.cloud/services).

## Contact

Should you have any questions or comments, don't hesitate to contact the Helmholtz Cloud team at <support@hifis.net>.
