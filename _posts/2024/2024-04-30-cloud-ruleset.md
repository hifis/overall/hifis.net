---
title: "The Long Journey to Clarity: The Legal Basis is in Place"
title_image: jonny-gios-SqjhKY9877M-unsplash.jpg
date: 2024-04-30
authors:
  - spicker
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Cloud
  - Legal Aspects
lang: en
lang_ref: 2024-04-30-cloud-ruleset
excerpt: >
  It took four years for the ruleset of the Helmholtz Cloud to meet various demands. Now, 18 Helmholtz Centres, along with the head office, have reached an agreement on the rules for service providers and user groups in the Helmholtz Cloud.
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Preparation for Sending the Ruleset"
    src="{% link /assets/img/posts/2024-04-29-cloud-ruleset/20240429_114347-1.jpg %}"
    />
  </div>
</div>

# The Long Journey to Clarity: The Legal Basis is in Place

**It took four years for the ruleset of the Helmholtz Cloud to meet various demands. Now, 18 Helmholtz Centres, along with the head office, have reached an agreement on the rules for service providers and user groups in the Helmholtz Cloud.**

## The Federation sets its own Rules
The idea of the Helmholtz Cloud is quite simple: Each Helmholtz Centre has the option to open selected services for use by other centres. 
All Helmholtz members agreed to run this federated cloud back in 2019. It leverages synergies both interdisciplinary in research and within IT departments, 
which is meaningful in a time of labour shortages and rising energy prices.

The technical implementation has become a masterpiece: The [Helmholtz AAI](https://hifis.net/aai) transfers the employee's login data to the scientific institution hosting the service in a secure and data protection-compliant manner. This novelty simplifies everyday tasks to such an extent that it's has 
become indispensable. It is making the over 30 services of the Helmholtz Cloud easily accessible.

However, when 19 scientific institutions jointly bundle services in a cloud, technical implementation alone is not sufficient. Legal aspects 
come into play: the scope of obligations must be limited, legal claims excluded, processes agreed upon. This may sound like a list of potential problems. Yet, the HIFIS team manages the feat of consistently focusing on the advantages of the Helmholtz Cloud.

## The Value-added Tax (VAT) Issue
*"And what about VAT, doesn't that apply to free IT services too?"* It is understandable that free services like YouTube, WhatsApp, or Dropbox (to name a few) pay VAT. Clarifying how the Helmholtz Cloud differs from these commercial entities took time and expertise. The distinction lies in the mission that Helmholtz pursues. It is an established German research organization, funded by the government. The purpose of research at all centers is independent science and there is no alignement on commercial success.

HIFIS brought together experts from legal and tax law fields and enlisted external consultants. Helmholtz's diversity, with 
two different ministries as funders and different legal forms, required careful research into the financing of the Helmholtz Cloud. Only on this detailed 
basis could the consulting firm provide a well-founded statement. Finally, VAT law and state aid law dictate new important clauses in the ruleset, 
resulting in: Usage of services is free of charge and of VAT for Helmholtz entities! External partners can be invited but need the leadership of a 
Helmholtz partner. Contract research is excluded, as is private or commercial use: Researchers may not use Helmholtz Cloud services for these purposes.

The validity of the regulations also marks the end of the set-up phase. The Helmholtz Cloud now begins a new era.

## Get in Contact
If you would like to learn more, please [contact]({% link contact.md %}) us. We have accumulated a wealth of experience over time and are happy to share it. 
However, the ruleset is a confidential document that will not be published.

## More information on this topic
If you are interested in this topic, please read [Legal Aspects of the Helmholtz Cloud - Interview](https://hifis.net/news/2024/09/20/interview-legal-aspects.html).

## Changelog

- 2024-06-24 Corrected typo.
- 2024-09-23 Add link to more information on this topic.