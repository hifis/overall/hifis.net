---
title: "Get impressions from our All-Hands & Friends Meeting!"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2024-09-17
authors:
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Event
excerpt: >
  We gathered for our HIFIS & Friends Meeting at DLR, Cologne: Time to update our stakeholders and advisors, as well as to cross-connect with our partners from
  Helmholtz and outside on numerous overarching topics to be considered on our way to "HIFIS2.0".
---

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right large nonuniform"
alt="Group Photo"
src="{% link assets/img/posts/2024-09-17-all-hands-meeting/all-hands-group.jpg %}"
/>
</div>
<div class="text-box" markdown="1">

# All-hands & Friends Meeting at DLR Cologne

On September 12-13, 2024, we welcomed all HIFIS members and close collaborators to our [All-hands & Friends meeting](https://events.hifis.net/event/1219/) at [German Center for Space and Aeronatics (DLR)](https://www.dlr.de) at its [site in Cologne](https://www.dlr.de/de/das-dlr/standorte-und-bueros/koeln/anreise-und-lage).

## Who participated?

Amongst the approx. 60 participants,
we were happy to see multiple members of our [scientific advisory](https://hifis.net/sab) and [federation](https://hifis.net/fedboard) boards,
including participants of NFDI, DFN, EOSC, EMBL, SURF, INFN, TU Dresden and more.
Also our colleagues from 
[Helmholtz Imaging](https://helmholtz-imaging.de/),
[Helmholtz AI](https://helmholtz.ai/) and
[Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/en)
were on site, which allowed us to join viewpoints and experiences on overarching topics that are related to all platforms of the 
[Helmholtz Information and Data Science Framework](https://www.helmholtz.de/en/research/challenges/information-data-science/).

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right tiny nonuniform"
alt="Presentation of discussion results"
src="{% link assets/img/posts/2024-09-17-all-hands-meeting/boards-presentation.jpg %}"
/>
</div>
<div class="text-box" markdown="1">

## What's been in there?

Firstly, we gathered to keep our stakeholders updated on the recent developments of HIFIS, including
new developments in our [cloud portfolio]({% post_url 2024/2024-07-17-new-services-connect-kg %}),
[service quality assurance]({% post_url 2024/2024-07-31-cloud-quality-areas %})
[Helmholtz Software Award]({% post_url 2024/2024-07-18-sw-award-final %})
and more.

More significantly, in multiple interactive sessions, we gathered valuable input from experts outside of the core HIFIS team on topics like

- Consulting 2.0, Mentoring, Scientific User Support,
- Operational Cybersecurity,
- Outreach and User Awareness,
- Anchoring in and Connection to Helmholtz Research Fields,
- E-Learning, 
- User Experience

and more.

Finally, the legal experts from Helmholtz Cloud Cluster have been interviewed with a user-centric perspective on **legal aspects when using Helmholtz Cloud**. 
Complex topics like tax issues, legal regulations, data protection frameworks etc are being considered by HIFIS, resulting in an evolving 
[Cloud Regulatory Framework]({% post_url 2024/2024-04-30-cloud-ruleset %}).
Scientists, however, should be confronted as little as possible with such topics.
In an interview, our experts shed some light on the scientist's perspective in using Helmholtz Cloud.
**[The interview has been published here]({% post_url 2024/2024-09-20-interview-legal-aspects %}).**

## Follow-up

The HIFIS team will summarize recommendations of our boards in a brief report.
Moreover, the ideas and proposal collected during our interactive sessions are pioritized to shape the upcoming works of "HIFIS 2.0".

## Stay tuned!

{% include subscription_lists.md %}

## Contact

Should you have any questions or comments, don't hesitate to contact the HIFIS team at <support@hifis.net>.

## Changelog

- 2024-09-20: Added link to meanwhile published blog post on interview.
