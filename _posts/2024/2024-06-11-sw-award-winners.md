---
title: "Upcoming: Results of Helmholtz Incubator Software Award"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-06-11
authors:
  - jandt
  - konrad
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Community
  - Cloud Services
  - Helmholtz RSD
excerpt: >
    The selection process for the awardees of the first round of the Helmholtz Incubator Software Prize has been finalized! The winners will be notified by June. Register or update your software project at the Helmholtz Research Software Directory (RSD) today!
    
---

# First round of Helmholtz Incubator Software Prize finalized!

The selection process for the awardees of the first round of the [Helmholtz Incubator Software Prize](https://os.helmholtz.de/en/open-research-software/helmholtz-incubator-software-award/) has been finalized.
The winners will be notified by June and a press release will be prepared.
They will be honored during the upcoming [Incubator Workshop](https://www.helmholtz.de/en/research/challenges/information-data-science/helmholtz-incubator/) in November.

The award is based on an initiative of 
[Helmholtz Federated IT Services (HIFIS)](https://www.hifis.net/), 
the [Helmholtz Information & Data Science Academy (HIDA)](https://www.helmholtz-hida.de/en/), 
the project ["Helmholtz Platform for Research Software Engineering - Preparatory Study" (HiRSE_PS)](https://www.helmholtz-hirse.de/), 
the [Helmholtz Open Science Office](https://os.helmholtz.de)
and funded by the President of the Helmholtz Association through the [Helmholtz Information and Data Science](https://www.helmholtz.de/forschung/im-fokus/information-data-science/) Framework.

## New Call upcoming soon - don't miss!

Due to the high success of this first round of the award, a second round is already in preparation. We hope to publish the new call by summer 2024.
So: Stay tuned!
Most straightforward, you can subscribe to our [newsletter](https://lists.desy.de/sympa/subscribe/hifis-newsletter) and [news feed](https://hifis.net/feed/news.xml).

## Register _your_ software in the Research Software Directory!

Register or update your software project at the [**Helmholtz Research Software Directory**](https://helmholtz.software) (RSD) today!
This service allows you to increase visibility of your project and cross-linking with other projects, organisations, contributors and publications.
The RSD is also part of the [Helmholtz Cloud](https://helmholtz.cloud/services/?serviceID=6f188651-a257-43fa-8878-0b7fb18d54b2).
Registration at the RSD is also precondition to take part in the next round(s) of the software award.

## Questions or proposals?

Contact us at <support@hifis.net>!

---

## Changelog

- 2024-07-18: Corrected typo.
