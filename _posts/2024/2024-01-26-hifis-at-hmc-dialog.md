---
title: "HIFIS at HMC Dialog"
title_image: chris-montgomery-smgTvepind4-unsplash.jpg
date: 2024-01-26
authors:
  - servan
  - jandt
  - Rhee, Franz
layout: blogpost
categories:
  - News
tags:
  - HMC
  - Service Orchestration
  - Research Software Engineering
excerpt: >
  As a first in the series of incubator meetings, HMC met HIFIS for a privileged dialogue to know each other better.
---

Recently, HIFIS was invited to participate in the __[HMC](https://helmholtz-metadaten.de/en/) Dialog__. With 35 participants, we had the opportunity to present HIFIS and discuss ideas for more synergies between our platforms. This is the first of a series where all platforms will meet each other in the coming weeks. We look forward to these exchanges and the new ideas that will follow. The incubator platforms have already introduced a different atmosphere in Helmholtz: We are sharing much more than just a few years ago and these meetings are a good illustration of this changed wind.

Uwe first presented how HIFIS can support scientists along the entire research lifecycle, in particular with the [Helmholtz cloud services](https://helmholtz.cloud), but also with our consulting offers, be it for research software engineering or for deploying services in the cloud. Together with Franz, he also explained how HIFIS is increasingly focusing on the __orchestration__ of different services. This is indeed a next big step for us in HIFIS, where we want to interconnect services to support complete and flexible scientific workflows. We have already started, as shown in [a demonstrator we have created with the Imaging platform](https://www.hifis.net/use-case/2023/04/18/service-orchestration.html), where 6 different Helmholtz sites are involved in a distributed service orchestration. 

HMC already uses a lot of services from HIFIS: the Helmholtz ID, of course, but also as an example the [__HMC dashboard on FAIR and open data in Helmholtz__](https://fairdashboard.helmholtz-metadaten.de/de) is hosted in a docker-based service on the HDF cloud, which was set up together with HIFIS.

As the name "Dialog" suggests, the presentation was followed by a lively discussion, for which we offer here a few takeaways.

An interesting point was on the potential for collaboration between HMC and HIFIS on the distributed services orchestration. As workflows gain wider adoption, HIFIS could potentially not only process but also generate metadata, e.g. recording pertinent metadata about the execution of a workflow itself and thus improving the __provenance information__ of processed datasets. Another interesting aspect would be to define with the user community which metadata is needed for a specific workflow to work.
Both these aspects would offer added-value to scientists as well as an __incentive to better annotate__ their data to use the scientific workflows available through HIFIS. It was agreed it would be interesting to see HMC and HIFIS teaming up on this topic, starting maybe with a pilot project in the portfolio of workflows Franz is currently supporting. We will follow-up on this.

Another inquiry was made regarding the measurement and publication of __cloud service energy consumption__. While it is not currently measured nor displayed centrally, we should start by aggregating the information that is published by the service providers themselves. In the long term, this information could be used to optimize workflows based on environmental footprint. The [Hub Energy](https://helmholtz-metadaten.de/de/energie/uebersicht) was identified as a resource for further collaboration on energy consumption frameworks and related ontologies, e.g. for computer centres.

Additionally, there were questions on consulting, best practices, scalability, and more. We could provide relevant links and stress that our support team at __[support@hifis.net](mailto:support@hifis.net)__ is always available to address questions.

The discussion concluded with the suggestion that future services developed through HMC calls should be onboarded in the Helmholtz Cloud, whenever appropriate, to enhance their sustainability.

## <i class= "fas fa-address-book"></i> Get in contact

- For HIFIS: [HIFIS Support](mailto:support@hifis.de)
