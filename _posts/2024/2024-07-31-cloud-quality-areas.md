---
title: "Helmholtz Cloud Sets Quality Criteria for its Services"
title_image: 2024-06-05-Cloud-Video.jpg
date: 2024-07-31
authors:
  - spicker
  - holz
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Helmholtz Cloud
  - Quality
  - Services
excerpt: >
    Discover how the Helmholtz Cloud evaluates its digital services using new quality criteria and sets itself rules for high availability, accessibility and reliability for the daily needs of scientists.
---
The Helmholtz Cloud offers a variety of digital services that are essential for scientists in their daily work. Annual [service portfolio reviews](https://hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.6_Review-Process-for-the-Service-Portfolio/) ensure that service information is up to date, allowing users to trust that all published services are well-maintained and compliant with legal requirements such as GDPR and state aid law. Since every service needs to fulfill [defined exclusion criteria](https://hifis.net/doc/process-framework/0_Corresponding-files/HIFIS-Cloud-SP_Service-Selection-Criteria-v3.pdf), a minimum standard for service quality is already set during the [Service Onboarding](https://hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.2_Onboarding-Process-for-new-Services/). To ensure that these services meet high quality standards above what is absolutely required, the HIFIS Cloud team evaluates criteria to measure service quality during operation. 

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right small nonuniform"
    alt="Figure points to diagrams an sustainability "
    src="{% link assets/img/posts/2024-07-31-cloud-quality-areas/HIFIS_Sustainability.svg %}"
    />
  </div>
</div>

While everyone has a general sense of what constitutes good or bad quality, it is difficult to define these criteria in concrete terms (and consequently find objective ways to measure them). In order to measure service quality, the HIFIS Cloud team identified five key fields (criteria) that are relevant to seek information on service quality and can be measured somehow:

- High availability
- (Easy) accessibility
- Existing user documentation
- Being up-to-date (and updated regularly)
- Reliable backup & recovery

To get into action, the HIFIS Cloud team started to discuss on how the five key fields could be measured in an objective way.
However, defining the evaluation of each criterion can be tricky. For example, the team discussed whether a lengthy documentation is better than a concise one, and whether a new software version is preferable to an older version that maybe is performing very well.

Even though it takes time to settle, the criteria already identified are giving insights on service quality and will enriche the service information now being collected during the Onboarding Process in [Plony](https://plony.helmholtz.cloud/). By implementing this initial set of quality criteria, service providers gain guidance on which areas are crucial for improving their services. This marks the starting point for further iterations, where the details of quality criteria will be refined and expanded.

Anyway, it is not only important to have a look at every single service but also at the service portfolio as a whole. Consequently, the next step will be to release a qualitative assessment of the [Helmholtz Cloud service portfolio](https://plony.helmholtz.cloud/sc/public-service-portfolio). This ensures that the services offered are precisely tailored to the needs of the scientific community.

## Questions or proposals?

Contact our Service Portfolio Manager at <support@hifis.net>
