---
title: "Relaunch of the HIFIS Software Spotlights"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-06-21 12:00
authors:
 - meessen
 - hetenyi
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Community
  - Cloud Services
  - Helmholtz RSD
excerpt: >
  The HIFIS Software Spotlights fully migrated to the RSD and relaunching soon.
---

We are happy to announce some exciting developments around the RSD and the HIFIS Software Spotlights.

### Migration finalised

As part of our efforts to increase the visibility of Research Software, we have now fully migrated the HIFIS Software Spotlights from their previous home at hifis.net/spotlights to a new, dedicated place at the [Helmholtz RSD](https://helmholtz.software/spotlights).


### Relaunch

Moreover, we are also relaunching the HIFIS Software Spotlights with a new format. Starting soon, the developers of the Spotlights will present their software on a monthly basis during the HIFIS Software Community meetings.

Stay tuned for invitations and more details on the upcoming HIFIS Software Spotlights.
