---
title: "Helmholtz Open Science Forum: Research Software"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2024-01-23
authors:
  - Gey, Ronny
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
excerpt: >
  Helmholtz Open Science Forum: Research Software, Feb. 05-06 @ UFZ, Leipzig
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right large nonuniform"
    alt="Helmholtz Open Science Forum: Research Software"
    src="{% link assets/img/posts/2024-01-23-forum-research-software/undraw_Programming_re_kg9v.png %}"
    />
  </div>
</div>

# Helmholtz Open Science Forum: Research Software

The [Task Group Research Software](https://os.helmholtz.de/en/open-science-in-helmholtz/working-group-open-science/task-group-research-software/) of the WG Open Science, the [Helmholtz Open Science Office](https://os.helmholtz.de) and the [Helmholtz Center for Environmental Research (UFZ)](https://www.ufz.de) co-organize the [5th Helmholtz Open Science Forum: Research Software](https://events.hifis.net/event/1192/). The event will be hosted by **UFZ in Leipzig Feb 05/06, 2024**.

## Topics and Timetable  

The 5th iteration of the Forum will feature topics such as the Helmholtz Incubator Software Award and Open Source Policy Offices as well as presentations of current software projects from within Helmholtz. Furthermore, we will gain insight into some recently published software policies. There is also room to speak about reusable CI/CD templates and workflows in GitLab.

The detailed programme can be found [here](https://events.hifis.net/event/1192/timetable/#20240205.detailed).
  
## Registration  

Please [**register here**](https://events.hifis.net/event/1192/). Participation is free of charge.
  
In case of questions please do not hesitate to contact <open-science@helmholtz.de>.
