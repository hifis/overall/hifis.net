---
title: "Think Tank on the Service Onboarding"
title_image: 2024-06-05-Cloud-Video.jpg
date: 2024-11-26
authors:
  - "Spicker, Annette"
  - "Holz, Laura Marie"
  - "Giesler, Andre"
  - "Albrecht, Marcus"
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Cloud
  - Process Framework
  - Service Integration
excerpt: >
  Do new Helmholtz Cloud services go online efficiently? The HIFIS Cloud Team reviewed its Onboarding process.
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Pinboard with new ideas around onboarding"
    src="{% link assets/img/posts/2024-11-26-think-tank-service-onboarding/pinboard.jpg %}"
    />
  </div>
</div>

# Do new Helmholtz cloud services go online efficiently?

More than 40 services are now available in the [Helmholtz Cloud](https://helmholtz.cloud/). But how well does it actually work to bring a new service to the Cloud Portal? The path follows a clearly defined process that is described in detail in the [Process Framework for Helmholtz Cloud Service Portfolio](https://www.hifis.net/doc/process-framework/Chapter-Overview/).

For the HIFIS team, the Onboarding process has become routine. They competently guide the new services through all necessary steps. For the service providers, on the other hand, it is usually a challenge: the process involves numerous tasks and requirements. Often, the technical integration has to be customized to fit the service.

To optimize this process, the Service Portfolio Management and Service Integration teams met in Jülich in September 2024 for a workshop. The aim was to critically and constructively identify ways to optimize the entire workflow.

# Concrete improvements for more efficient Onboarding

Numerous ideas were collected in several iterations of the group work. From these, a series of concrete tasks have now been formulated that the Helmholtz Cloud team will implement successively, including:
* **A step indicator** will provide visual support for entering the required service information in the [Plony database](https://plony.helmholtz.cloud/).
* **A preview of the service card** function allows to see early how the service is displayed in the Cloud Portal. 
* **The optimization of the structure and scope of service information** requires a revision to reduce the effort for all parties involved.
* **Personal contact** is suggested earlier: in addition to the central support address <support@hifis.net>, a [public chat channel](https://mattermost.hzdr.de/hifis-community/channels/town-square) is already available to enable direct communication at an early stage.

With these and other measures, the team will make the Onboarding of services into Helmholtz Cloud more efficient and user-friendly for service providers and everyone involved.

## Getting Started

You want to publish your service in the Helmholtz Cloud? Have a look at our [**documentation for Service Providers**](https://hifis.net/doc/helmholtz-cloud/providers/getting-started/). 

## Contact

Should you have any questions or comments, don't hesitate to contact the Helmholtz Cloud team at <support@hifis.net>.
