---
title: "Check out two new scientific services!"
title_image: photo-of-open-signage-2763246.jpg
date: 2024-07-17
authors:
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Helmholtz Cloud
  - Science
  - Metadata Repositories
excerpt: >
    New scientific services in Helmholtz Cloud: Check out Helmholtz Imaging's CONNECT platform and HMC's Knowledge Graph service and join our community!
---

Good news!
Recently we onboarded two new [scientific metadata services](https://helmholtz.cloud/services/?filterCategory=Metadata%2520Repository) to our Helmholtz Cloud portfolio that have been developed by our sister platforms:

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right micro nonuniform"
    alt="Logo of Helmholtz Imaging CONNECT service"
    src="{% link assets/img/posts/2024-07-17-new-services-connect-kg/helmholtz-connect-logo.svg %}"
    />
  </div>
</div>

## CONNECT with Helmholtz' imaging capabilities!

[**Helmholtz Imaging CONNECT**](https://helmholtz.cloud/services/?serviceID=39625088-be26-4cd1-aa3a-410c40cb26d6) is a platform showcasing the diverse spectrum of scientific imaging within the Helmholtz Association. Scientists develop and apply numerous imaging modalities to study processes at atomic, molecular, organismic, ecosystemic, and cosmic scales. This imaging network facilitates collaboration, offering a space to connect experts, instruments, and applications. The service is being developed by [Helmholtz Imaging](https://helmholtz-imaging.de/) platform.

**You're invited to join!** Scientists affiliated with Helmholtz Centers are encouraged to join by adding their profiles, modalities, applications, and stories to expand their network and explore new research opportunities.


<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right tiny nonuniform"
    alt="Logo of Helmholtz KG service"
    src="{% link assets/img/posts/2024-07-17-new-services-connect-kg/helmholtz-kg-logo.png %}"
    />
  </div>
</div>

## Knowing what Helmholtz knows: Knowledge Graph

The [**Helmholtz Knowledge Graph (KG)**](https://helmholtz.cloud/services/?serviceID=6cc16403-f1ec-4aa5-96e5-afd01455c1ce) is a central metadata repository for digital assets of the Helmholtz community including data, publications, code, institutions, and experts. It is an openly published service that makes the Helmholtz Association's research data findable and usable according to the FAIR principles (Findable, Accessible, Interoperable, Reusable). The sources are metadata from various data providers such as repositories, libraries, and code deposits.
The Helmholtz KG is developed by unified Helmholtz Data and Information Exchange (unHIDE) initiative of the [Helmholtz Metadata Collaboration (HMC)](https://helmholtz-metadaten.de/) that aims to harmonise and consolidate the representation of digital assets within the Helmholtz association.

**Visit Helmholtz KG's search interface and explore the knowledge of Helmholtz** through keyword, category browsing or filtered searches.

## Questions or proposals?

Contact our Cloud Team at <support@hifis.net>
