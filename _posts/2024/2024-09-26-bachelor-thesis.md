---
title: "Bachelor Thesis on Resource Booking in the Helmholtz Cloud"
title_image: sergi-kabrera-2xU7rYxsTiM-unsplash.jpg
data: 2024-09-26
authors:
 - "Tallarek, Benjamin"
 - "Glodowski, Tempest"
layout: blogpost
additional_css:
  - image-scaling.css
categories:
 - News
tags:
 - Helmholtz Cloud
 - Resource Booking
 - Bachelor
excerpt: >
   The HIFIS team supervised a bachelor's thesis, aiming at the design and implementation of the process for booking resources in Helmholtz Cloud Portal.
---

<div class="floating-boxes">
<div class="image-box align-right">

<img class="right large nonuniform"
alt="Screenshot of resource package form in plony"
src="{% link assets/img/posts/2024-09-26-bachelor-thesis/form.png %}"
/>
</div>
</div>
<div class="text-box" markdown="1"> 

# New Process Will Improve the Booking of Resources in the Helmholtz Cloud

In the Helmholtz Cloud the centers offers selected services for the members of the Helmholtz Association. As part of the Helmholtz Cloud Portal, a central system is required which allows the booking of resources for the services based on customizable rules. This system is divided into multiple parts: the [Helmholtz Cloud Portal](https://helmholtz.cloud/) represents the interface to users, where the resource booking takes place. The resource booking rules are stored in the service data management system [Plony](https://plony.helmholtz.cloud/).

The task of this bachelor thesis was to first develop a process for resource booking and then implement it as a web application in Plony. As part of the process development, an UML activity diagram was created which served as the basis for the implementation.

Plony consists of a frontend and a backend. The backend was developed with Plone CMS in the programming language Python, the frontend with JavaScript and the framework Vue.js. The application includes automatic validation of user entries and an approval process to ensure that no incorrect resource bookings are made.

As a result, the process was successfully developed and implemented that meets the given requirements. On this base, the application will be expanded to include additional functionalities that simplify the creation of resource booking rules for users.

## Questions? Comments? Proposals?
If you have anything to tell us, don't hesitate to [contact us](mailto:support@hifis.net).

## Changelog

- 2024-10-10: Shortened title.
