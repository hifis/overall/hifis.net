---
title: "Apply for the Helmholtz Software Award 2024!"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-09-05
authors:
  - konrad
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Community
  - Software Services
  - Helmholtz RSD
excerpt: >
    On August 20, 2024, the call for applications for the Award 2024 was published and sent to all Helmholtz Centers.
    The nomination deadline is October 20th 2024!
    
---

# Second Round of the Helmholtz Software Award has started!

The Helmholtz Research Software Award is intended to promote the development of professional, high-quality and sustainable research software and to recognize the commitment to software as the basis of modern data science. Recently we [informed you on the winners of the first round of the award in 2023]({% post_url 2024/2024-07-18-sw-award-final %}). 
On August 20, 2024, the [call for applications for the Award 2024]({% link assets/documents/2024-Helmholtz-Software-Award-Call.pdf %}) was published and sent to all Helmholtz Centers. As before, the prize is awarded in three categories for 

1. the best scientific software, 
2. sustainability and 
3. newcomer. 

The nomination deadline is October 20<sup>th</sup> 2024!

If you have any questions or suggestions about the award, please contact <open-science@helmholtz.de> or <support@hifis.net>.

## Register _your_ software in the Research Software Directory!

Register or update your software project at the [**Helmholtz Research Software Directory**](https://helmholtz.software) (RSD) today!
This service allows you to increase visibility of your project and cross-linking with other projects, organisations, contributors and publications.
The RSD is also part of the [Helmholtz Cloud](https://helmholtz.cloud/services/?serviceID=6f188651-a257-43fa-8878-0b7fb18d54b2).
Registration at the RSD is also precondition to take part in the next round(s) of the software award.

## Don't miss any news on good scientific software in Helmholtz - and HIFIS!

Most straightforward, you can subscribe to our [newsletter](https://lists.desy.de/sympa/subscribe/hifis-newsletter) and [news feed](https://hifis.net/feed/news.xml).

## Questions or proposals?

Contact us at <support@hifis.net>!
