---
title: "Legal Aspects of the Helmholtz Cloud - Interview"
title_image: 2024-06-05-Cloud-Video.jpg
date: 2024-09-20
authors:
  - spicker
  - Glodowski, Tempest
  - erxleben
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Helmholtz Cloud
  - Legal Aspects
  - Interview
excerpt: >
  What's possible unter a legal point of view? During the HIFIS & Friends meeting at DLR, the Helmholtz Cloud team for Legal Aspects team answered questions in an interview.
---
# What's possible under a legal point of view?  
**Interview at [HIFIS & friends meeting at DLR]({% post_url 2024/2024-09-17-all-hands-meeting %}), 2024-09-12**

*At the HIFIS & Friends meeting at DLR, a lively discussion emerged as T. Glodowski and A. Spicker of  HZB delved into a stimulating discussion on the legal aspects of the [Helmholtz Cloud](https://helmholtz.cloud/). Moderated by F. Erxleben (HZDR), the interview quickly highlighted the complexity and relevance of the topic. Audience questions enriched the discussion, proving that this open format was the effective way to explore the legal challenges and opportunities from a scientific user's point of view.*

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right medium nonuniform"
    alt="Figure points to diagrams an sustainability "
    src="{% link assets/img/posts/2024-09-12-interview-legal-aspects/2024-09-12-HIFIS-DLR-Interview.jpg %}"
    />
  </div>
</div>

### (F) As the Manager for Cloud Services, you are in charge of the Helmholtz Cloud. What kind of services are offered here?

(A) The Helmholtz Cloud is designed to support scientists with digital services in their daily business. So, our aim is to offer a high-quality portfolio of resources from the researcher’s perspective, covering the complete scientific project lifecycle: This starts with writing the proposal, continues with measuring and computing the data and culminates in publishing the research results.

During the ramp-up phase, we successfully published a lot of collaboration tools including chat, GitLab, computing. Now I’m pleased to state that we are entering  "the next level", so to speak. Especially the [Earth and Environment DataHub](https://earth-data.de/) and namely the GFZ have handed in a lot of scientific services, such as the Earthquake Explorer or Earth Gravity tools. If this trend continues, the Helmholtz Cloud will get a completly different scope.

### (F) Who can use the Helmholtz Cloud?

(T) The Idea of the Helmholtz Cloud is to share already existing resources from the centers with all other Helmholtz centers. Every center can provide some of its local resources in the Helmholtz Cloud. So, everyone in Helmholtz can use the available resources for their collaborative work. And for this you can also invite external colleagues to their projects.

### (F) Does that mean that everyone can use everyone’s services?

(A) Our vision is to enable collaboration between scientists as seamlessly as possible. However, of course, we have to deal with the circumstances. 
Firstly, we can only share resources within Helmholtz. Fortunately, this includes external partners that are invited to Helmholtz-based collaborations. Although the technical capabilities would allow a broader scope, we are not entitled to expand to a wider audience. 

Let me explain that. The funding of the Helmholtz Centers comes from the government. And it’s strictly dedicated to science. Therefore, the centers are not allowed to use their funds for offering services to a public audience. This would undermine the public market. 

It is only because the Helmholtz Association organizes all the individual Helmholtz Centers into a larger unit that we can share the cloud resources for free. We had to take a detailed look in the funding conditions of the centers so that the sharing of resources within the community was acknowledged as being in line with the purpose of funding. 

### (F) Are there any other aspects of law that come into play when offering such resources?

(T) Oh yes. Plenty! Right from the beginning, we were aware of the data protection aspect, the European GDPR. But it quickly became clear that as a cloud provider, you have to deal with many areas of law. In addition to Data Protection Law (GDPR/DSGVO), we have also dealt with topics such as State Aid Law (EU Beihilferecht) and Tax Law (Steuerrecht). In order to be legally compliant here, a further agreement had to be drawn up: the Ruleset of the Helmholtz Cloud (Regelwerk). This agreement rules responsibilities and obligations for each role within the Helmholtz Cloud. These are using and providing Helmholtz Centers and HIFIS itself.

### (F) This sounds like quite the challenge. Do all users have to contend with such issues if they want to use these resources?

(A) Well, our aim is to shield the users from this. They should be confident that HIFIS is taking care of the legal aspects and guide them carefully through the necessary processes.

We negotiated with all stakeholders to establish the legal base agreements. As you know, we have 18 Helmholtz Centers and the head office. And we have to persuade legal department, data protection officer and tax expert of each center. One reason why we finally were successful is that everyone was inspired by the vision of the Helmholtz Cloud.

### (F) Could you give an example how this affects the daily work of the users?

(T) For example, as a user - in most cases - you do not need to care about GDPR. Because it’s a Helmholtz Cloud service, we as HIFIS already cared about the GDPR requirements. Therefore we have created a Joint Controller Agreement in close cooperation with the 19 Data Protection Officers of the centers. It covers all the components we need for operation the Helmholtz Cloud. For these four "Core Components" the agreement specifies, which personal data is processed and which center is responsible for this. The running of the Helmholtz Cloud is GDPR compliant and from the user perspective this is all behind the scenes.

### (F) Are there any limitations?

(A) Legally, it is forbidden to use the Helmholtz Cloud services for economic or private purposes. And this is because of the State Aid Law (EU Beihilferecht). To simplify this, you can say that one can use our services for all tasks that are funded by the government. 

### (F) How can I then work with my non-Helmholtz colleagues especially outside the EU e.g. CERN?

(T) If you want to collaborate with non-Helmholtz colleagues you will need to register a so called Virtual Organisation (VO). VO is only the technical term. You can think of it as the technical representation of real groups as project groups or research groups. So all members of your research group have to join the new VO and accept the corresponding policies. Then service providers can enable their services for this VO and every member can use the service. So the VO Manager has to be an employee of Helmholtz. And he/she is responsible for the invitation of other people like external partners.

### (F) Who can I ask for help?

(A) There are many starting points: First of all you can have a look at the [Helmholtz Cloud](https://helmholtz.cloud/) portal. For legal questions, you can ask your local legal department, your data protection officer. And of course, you can ask us.

### (F) How to reach you?
(T) The easiest way is to open a ticket via sending an email to <support@hifis.net>.  
This is our single point of contact and we will make sure that your request reaches the right expert as fast as possible.

### (F) And what are the next steps for your working group?

(A) We are currently putting our ruleset into practice. Now that all Helmholtz Centers has agreed, we are going to publish the [Terms of Use](https://helmholtz.cloud/terms-of-use). It is our duty to regularly inform our users that they are not allowed to use the cloud services for private or economic purposes. For example, you can find a banner on the Helmholtz Cloud website, and soon you will have to confirm the Terms of Use via the Helmholtz ID.

(T) Additionally, we need to improve the process of data processing agreements (AVVs) between using and providing centers. Here, GDPR becomes complex because it stipulates that the using center is fully responsible for the data processing. However, in reality, the providing center holds all the information and opportunities.

Currently we are working on another GDPR (DSGVO) agreement, which should define the rules for the processing of personal data between the service providers and the centers using their services.

### (F) What if I want to use these services in an economic setting, e.g. contractual research, startups?

(A) At the moment, we cannot allow a commercial use of the Helmholtz Cloud services. The possible solution is to make your own agreements with the providing center. Of course, the providing center can decide to offer a part of its resources for economic purposes. But that’s up to a specific agreement and beyond our scope.

### (F) Any last-minute advice/ comments?

(T) If you want to offer a service in Helmholtz, let us help you.

## More information on this topic
If you are interested in this topic, please read [The Long Journey to Clarity: The Legal Basis is in Place](https://hifis.net/news/2024/04/30/cloud-ruleset.html).