---
title: "Prototype implementation of the NEST Desktop"
title_image: jamie-templeton-6gQjPGx1uQw-unsplash.jpg
date: 2024-01-24
authors:
  - rhee
  - servan
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - Use-Case
tags:
  - Helmholtz Cloud
  - Data Science
  - Rancher managed Kubernetes
  - in development
excerpt: >
  The open source NEST desktop from E-BRAINS can now run on Helmholtz Cloud infrastrusture, eliminating the need for programming skills and complex installations, and providing a user-friendly experience through a GUI accessible in a web browser.
---

## The NEST Desktop: Computational neuroscience for students

NEST Desktop is an educational application for neuroscience that serves as a web-based graphical user interface (GUI) for the [NEST Simulator](https://helmholtz.software/software/nest) — a tool for spiking neuronal network models. NEST Desktop eliminates the need for programming skills and complex installations, providing a user-friendly experience through a GUI accessible in a web browser.

NEST Desktop offers features for creating, configuring, and simulating neuronal network models, as well as visualizing and analyzing results. It consists of two main views, allowing users to interactively construct point neuron network models using a visual modeling language.

The architecture ensures robustness against technological changes and simplifies deployment for both students and teachers. The tool enhances the teaching of computational neuroscience in university courses, promoting equal opportunities by being available on public resources like the European ICT infrastructure for neuroscience, EBRAINS, and more recently, on resources provided via HIFIS for [Helmholtz Cloud](https://helmholtz.cloud).
The NEST Desktop powered by HIFIS is accessible through the following link: [NEST Desktop - HIFIS](https://nest-desktop.readthedocs.io/en/dev/online-services/helmholtz-cloud.html).
 
<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right large nonuniform"
    alt="NEST Desktop deployment on Kubernetes"
    src="{% link /assets/img/posts/2023-11-24-uc-nest/uc_nest_k8s_deployment.png %}"
    />
  </div>
</div>

## NEST on Helmholtz Cloud's Rancher service

The deployment architecture of the NEST Desktop service comprises three integral components, that have been implemented in a [Rancher/Kubernetes](https://helmholtz.cloud/services/?serviceID=8da9d670-383c-4641-9896-fa25220cc0b5) environment that is provided via [Helmholtz Cloud](https://helmholtz.cloud):

- **NEST Simulator:** This component is responsible for executing simulations of spiking neural network models. It leverages a highly optimized simulation kernel written in C++.
- **NEST Desktop:** This is a web-based application designed to offer a user-friendly graphical interface for the NEST Simulator.
- **HTTP Server:** The HTTP Server serves as the unit for delivering the NEST Desktop service to client programs and their users. Notably, it integrates an authentication and authorization module. This module implements the OpenID Connect Relying Party functionality to feature the login via Helmholtz ID.

For each of the three components, the following Kubernetes resources have been established:

- **Pod**: In Kubernetes, a pod represents a co-located group of containers and serves as the fundamental building block. Instead of deploying containers individually, a pod facilitates the deployment and management of a cohesive group of containers as a single unit.
- **Service**: A Kubernetes service is an object designed to provide a stable, singular access point to a set of pods that deliver identical functionality.
- **Deployment**: The Kubernetes Deployment object is responsible in managing a group of pods within a workload. It oversees the creation and deletion of pods and allows for the selection of an update strategy. This means that rather than manually executing a series of operations to replace a set of Pods with ones running an updated application version, updates to the configuration in the Deployment object trigger Kubernetes to automate the update process.


## <i class= "fas fa-address-book"></i> Get in contact

- For NEST: <https://helmholtz.software/software/nest>
- For HIFIS and the Helmholtz Cloud Services: [HIFIS Support](mailto:support@hifis.de)
