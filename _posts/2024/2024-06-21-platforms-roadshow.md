---
title: "Working with scientific data? Join our Roadshow!"
title_image: jason-goodman-Oalh2MojUuk-unsplash.jpg
date: 2024-06-21 18:00
authors:
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Education
  - Events
excerpt: >
    Are you working with scientific data? Then don’t miss the virtual roadshow organized by the five Helmholtz Information and Data Science platforms. Register now!
---

<div class="floating-boxes">
  <div class="image-box align-right">
    <img class="right large nonuniform"
    alt="Platforms Roadshow Teaser"
    src="{% link /assets/img/posts/2024-06-21-platforms-roadshow/DataScience-Roadshow-indico (840 x 250 px).png %}"
    />
  </div>
</div>

# Are you working with scientific data?

Don’t miss the virtual roadshow organized by the five Helmholtz Information and Data Science platforms HIFIS, Helmholtz AI, Helmholtz Imaging, HMC and HIDA!

- **When?** June 26th 10 – 11:30 am 
- **Where?** Online 
- [**Register here!**](https://events.hifis.net/event/1609/registrations/1629/) <i class="fa fa-arrow-left" aria-hidden="true"></i> <i class="fa fa-arrow-left" aria-hidden="true"></i> <i class="fa fa-arrow-left" aria-hidden="true"></i>

**Discover our huge range of data science solutions and support - don't miss out!**

The roadshow consists of a short presentation of the portfolios followed by engaging breakout sessions: 

- Interact with experts: Engage directly with representatives and scientists from each platform to gain insights into their offers. 
- Learn and collaborate: Discover how each platform's services and expertise can support and enhance your research.
- Network with peers: Connect with fellow researchers and explore potential collaborations within the Helmholtz Association.

**We'd love to see you there!**
