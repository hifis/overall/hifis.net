---
title: "Software Award: Winners 2023 and Call 2024"
title_image: pexels-cottonbro-5483071.jpg
date: 2024-07-18
authors:
  - konrad
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Announcement
  - Community
  - Software Services
  - Helmholtz RSD
excerpt: >
    The winners of the first Helmholtz Software Award (2023/2024) have been presented. 
    The Scientific Originality Prize was given to CellRank from HMGU. 
    The Sustainability Prize was given to ESMValTool from DLR and
    the Newcomer Prize was given to openCarp from KIT!
    
---

# First Helmholtz Software Award has been presented!

It's finally done: the winners of the first Helmholtz Software Award have been found. A total of 42 proposals from 16 centres were received. These submissions have been evaluated by international experts. On this basis, a committee consisting of representatives from the participating incubator platforms and the Helmholtz Open Science Office made the final selection of the prize winners for the three categories. The Helmholtz President, Prof. Dr. Otmar D. Wiestler, informed the respective centres on July 15, 2024.
## And the winners are: 
 
1. Scientific Originality Prize (Main Award, EUR 5,000):  
   **CellRank (dynamics from multi-view single-cell data)**  
   <https://helmholtz.software/software/cellrank>  
   Representative: Philipp Weiler (HMGU)
 
2. Sustainability Prize (3,000 EUR):  
   **ESMValTool (Earth System Model Evaluation Tool)**  
   <https://helmholtz.software/software/earth-system-model-evaluation-tool-esmvaltool>  
   Representative: Birgit Hassler (DLR)
 
 
3. Newcomer Prize (EUR 2,000):  
   **openCARP (Cardiac Electrophysiology Simulator)**  
   <https://helmholtz.software/software/opencarp>  
   Representative: Axel Löwe (KIT)

Congratulations to the developers and software engineers and thank you for these fantastic software applications! 

## New Call upcoming soon - don't miss!

The call for 2024 is already prepared. It will be published within the next days with an application deadline end of September. So: Stay tuned!

Most straightforward, you can subscribe to our [newsletter](https://lists.desy.de/sympa/subscribe/hifis-newsletter) and [news feed](https://hifis.net/feed/news.xml).

## Register _your_ software in the Research Software Directory!

Register or update your software project at the [**Helmholtz Research Software Directory**](https://helmholtz.software) (RSD) today!
This service allows you to increase visibility of your project and cross-linking with other projects, organisations, contributors and publications.
The RSD is also part of the [Helmholtz Cloud](https://helmholtz.cloud/services/?serviceID=6f188651-a257-43fa-8878-0b7fb18d54b2).
Registration at the RSD is also precondition to take part in the next round(s) of the software award.

## Questions or proposals?

Contact us at <support@hifis.net>!

## Changelog

- 2024-09-24: completed members of committee
