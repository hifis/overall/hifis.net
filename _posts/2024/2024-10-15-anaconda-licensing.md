---
title: "Anaconda Restrictions and Alternatives for RSD"
title_image: sergi-kabrera-2xU7rYxsTiM-unsplash.jpg
data: 2024-10-15
authors:
 - "Juckeland, Guido"
 - erxleben
layout: blogpost
categories:
 - News
tags:
 - RSE
 - Licensing
 - PSA
excerpt: >
   Anaconda is very popular in research software development. 
   But the current licensing policy has far-reaching consequences for all Helmholtz centers.
---

# Restricted Usage of Anaconda

The company Anaconda provides very convenient software development environments and libraries for Python and R, especially for beginners. 
Due to their many years of free availability for research and educational institutions, they are also very popular in training and in the discussion on StackOverflow.

In a clarification of the terms of use amended in 2020, Anaconda also designates research institutions with more than 200 employees as commercial users, for which corresponding usage fees apply.
These fees are also due if employees of the institutions access Anaconda's software directories.
It is known from several institutions that this is actually being pursued by Anaconda.

As a consequence, some Helmholtz centers (e.g. HZDR) have decided to prohibit use and block access to the software directories, 
including access restriction to all software obtained directly from Anaconda, and
in particular the standard channels in the still freely available `conda` command line environment.

However, the use of `conda-forge` (without the default channels) is still possible. 

## Recommended Alternatives

The following alternatives are available for the components offered by Anaconda:

* [mamba](https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html) instead of `conda`
* [Miniforge](https://github.com/conda-forge/miniforge) which defaults to `conda-forge` instead of the `anaconda` channel
* [Poetry](https://python-poetry.org/) project and package management solution for Python
* [Rye](https://rye.astral.sh/) Python packaging and dependency management
* [uv](https://docs.astral.sh/uv/) Fast Python package and project manager, written in Rust
* [pip](https://pypi.org/project/pip/) as Python package manager
* [venv](https://docs.python.org/3/library/venv.html) for managing virtual Python environments
* [Spyder](https://github.com/spyder-ide/spyder/releases) can also be installed directly.
* Under Windows, [WinPython](https://winpython.github.io/) is also an alternative complete Python environment.

## Questions? Comments?

If you have anything to tell us, don't hesitate to [contact us](mailto:support@hifis.net).

## Further Reading

Individual Helmholtz centers have contributed their own guidelines on this topic, including:

* [Recommendation of the UFZ](https://codebase.helmholtz.cloud/m.bernt/conda-recommendations/) by Matthias Bernt
* [Recommendation of the FZJ-JSC](https://www.fz-juelich.de/en/rse/the_latest/the-anaconda-is-squeezing-us) by Robert Speck

## Changelog

- 2024-10-16: Added one recommendation
- 2025-01-29: Added further reading and clarified a key point
