---
title: "HIFIS Evaluation: First feedback (updated)"
title_image: jingda-chen-4F4B8ohLMX0-unsplash.jpg
data: 2022-12-12
authors:
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
redirect_from:
  - news/2022/10/06/hifis-evaluation
categories:
  - News
tags:
  - Evaluation
  - Report
excerpt: >
  After three years of ramp-up, HIFIS underwent a thorough evaluation by an international expert commitee. Now, the final results are here: As already indicated, we got a very positive feedback and recommendations to continue our work.


---

# Evaluation after first years of ramp-up

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right large nonuniform"
alt="webODV logo"
src="{% link assets/img/posts/2022-10-06-hifis-evaluation/HIFIS_Eval.jpg %}"
/>
</div>
<div class="text-box" markdown="1">
HIFIS, the Helmholtz-wide platform for federated IT services, underwent a thorough review during August and September 2022, with the [major event taking place at the coordinating centre DESY from Sept 29–30](https://events.hifis.net/event/450).

The evaluation committee represents national and international institutions and collaborations, such as the
[European Open Science Cloud](https://www.eosc-portal.eu/)
and multiple more.
External viewpoints, e.g. from
[Open Science Office](https://os.helmholtz.de/en/),
[Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/),
[NFDI](https://www.nfdi.de/), and
[DFN](https://www.dfn.de/),
as well as in-depth discussions with members of the [HIFIS Scientific Advisory Board]({% link structure/members-sab.md %}),
helped the reviewers to obtain a possibly complete picture of the landscape where HIFIS is embedded into, and its interfaces to other digital initiatives.

</div>
</div>
<div class="clear"></div>

## What has been presented

During two day's sessions, the [progress of HIFIS](https://events.hifis.net/event/450/timetable/#20220929.detailed), as well as the
[plans for potential future development](https://events.hifis.net/event/450/timetable/#20220930.detailed),
have been presented and discussed in detail.
HIFIS has been establishing the technical and procedural frameworks to allow all Helmholtz Centres to seamlessly share digital resources with all Helmholtz and collaboration partners.
Meanwhile, more than 20 cloud services have been brought online, with more than [11k users registered, numbers strongly rising](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/722376/artifacts/file/plots/202102_Backbone/plot_2.pdf).
HIFIS education courses were received comparably well, with more than [14k overall participant hours so far](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/722376/artifacts/file/plots/pdf/software_workshops-plot_3.pdf).

## Initial feedback

Already during the review event, a first response by the reviewers has been formulated:
The achieved progress of all three HIFIS clusters has been rated excellent.
Multiple of the future plans of further strengthening the HIFIS portfolio have been strongly supported.
The enthusiasm of the HIFIS team, spanning practical and efficient cooperation throughout Helmholtz, has been especially acknowledged.

### Post-review meeting for all stakeholders of HIFIS

On **Nov 18**, we held an online [**Post Review Meeting**]({% post_url 2022/2022-11-21-HIFIS-meeting %}).
All Helmholtz and HIFIS stakeholders were invited to discuss with us the results and next steps.

## Final results and next steps
Now, the final results are in: They substantiate the initial, very positive feedback and clearly recommend the continuation of HIFIS. 
The report also gives us advice on some aspects to further elaborate on throughout the next months, in order to set them into place.

The [written report](https://syncnshare.desy.de/index.php/f/467281196) is available within Helmholtz (login required).

As mentioned in our earlier post, we will derive action points for HIFIS, adopting our next steps and [roadmap]({% link timeline/index.md %}) according to reviewer's suggestions.
Once they are published, we will talk about our plans for 2023 in a blog post, so stay tuned!

## Comments? Suggestions?
Contact us anytime if you have any suggestions or questions: <support@hifis.net>

---

#### Changelog
- 2022-12-12 -- Updates with respect to meanwhile finished written feedback report; added link.
- 2023-05-09 -- Removed outdated references to initial feedback.
