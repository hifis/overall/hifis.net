---
title: "Happy holidays and all the best to you!"
title_image: andrea-music-2RFhuLEL8NI-unsplash.jpg
data: 2022-12-15
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Newsletter
excerpt: >
  We would like to thank all our users, partners and friends for a great cooperation over the last year. We are looking forward to set new milestones and master new challenges together. But for now, we wish you all happy holidays and all the best to you in the year to come!


---
<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="blue baubles on a tree"
src="{% link newsletter/2022-12-xmas/andrea-music-2RFhuLEL8NI-unsplash.jpg %}"
/>
</div>
<div class="text-box" markdown="1"> 


# As the year comes to an end, we want to look back
It has been a challenging year in many ways, but we have overcome a lot of obstacles by focusing on our strengths and joining forces wherever possible. In terms of IT services for science, we showed how this can be done with our platform HIFIS, to which the centres of the Helmholtz Association contribute their best solutions and competencies.

For a look back at our achievements during the last year, we invite you to our latest newsletter in [HTML](https://hifis.net/newsletter/2022-12-xmas/2022-12-HIFIS-Newsletter.html) or as [PDF](https://hifis.net/newsletter/2022-12-xmas/2022-12-HIFIS-Newsletter.pdf).

We would like to thank all our users, partners and friends for a great cooperation over the last year. We are looking forward to set new milestones and master new challenges together. But for now, we wish you all happy holidays and all the best to you in the year to come!

The HIFIS Team

# Interested in our plans for the next year?
Then don't miss our next newsletters, starting from January 2023, to track the progress and perspectives of HIFIS!
**[Subscribe here](https://lists.desy.de/sympa/subscribe/hifis-newsletter)**, and spread the word and link.
Check also our newsletters [archive]({% link newsletter/index.md %}) and [blog posts]({% link posts/index.html %}).
