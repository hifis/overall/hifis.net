---
title: "Register for the Incubator Summer Academy"
title_image: spacex-uj3hvdfQujI-unsplash.jpg
data: 2022-07-07
authors:
  - klaffki
layout: blogpost
categories:
  - News
tags:
  - Helmholtz Incubator
  - Event
excerpt: >
    Registry for the Incubator Summer Academy has opened. Discover workshops from beginner to expert level, a data challenge, lectures and more.
---

## From Zero to Hero — don't hesitate, register now!
As announced earlier, the [Incubator Summer Academy](https://events.hifis.net/event/398/) will take place on **September 12–23, 2022**.

<figure>
    <img src="{% link assets/img/posts/2022-05-20-save-the-date-summer-academy/Incubator_Summer_Academy_visual_broad.jpg %}" alt="colorful stripes" style="float:right;width:50%;">
</figure>

The five Incubator platforms
[Helmholtz.AI](https://www.helmholtz.ai),
[Helmholtz Imaging](https://www.helmholtz-imaging.de/),
HIFIS,
[HIDA](https://www.helmholtz-hida.de/) and
[HMC](https://helmholtz-metadaten.de) have teamed up to create an exciting program consisting of:

* Data science workshops from beginner to expert level,
* a data challenge,
* keynote speeches, and
* plenty of digital and on-site networking opportunities

**The registry has opened**, don't wait too long to register — the first classes are already full! You'll find the buttons "register" or "apply for participation" in [the workshop list](https://events.hifis.net/event/398/timetable/) or, below, in the description of each event.

From fundamental workshops like „Introduction to Python“ to advanced courses on „Machine Learning for Instance Segmentation and Tracking“, participants will be able to pick and choose offers that best suit their experience levels and interests. In our data challenge, you and your teammates can put your (newly acquired) knowledge to the test. You’ll get the chance to network with colleagues from all Helmholtz centers and learn more about career opportunities and data science-related exchange programs. Finally, don’t miss our lectures by data science experts!

The Incubator Summer Academy is open to all doctoral and postdoctoral researchers in the Helmholtz Association. Additionally, a small number of seats in our workshops are reserved for Master students, doctoral and postdoctoral students from other research institutions and universities.



