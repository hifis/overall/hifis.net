---
title: "Awesome list of Useful Links"
title_image: camylla-battani-ABVE1cyT7hk-unsplash.jpg
date: 2022-09-19
authors:
  - foerster
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Consulting
excerpt: >
    Let's share a comprehensive link list collection of helpful resources for software development.
---


<!--=======================-->
<!--== DIV: Floating Box ==-->
<!--=======================-->
<div class="floating-boxes">

<!------------->
<!---- DIV ---->
<!------------->
<div class="image-box align-right">
  <a href="https://github.com/hifis-net/awesome-rse">
  <img class="right medium nonuniform"
         alt="Awesome List Logo"
         style="min-width: 270px; max-width: 270px;"
         src="{% link assets/img/posts/2022-09-19-awesome-list/awesome-list-logo-helmholtz.svg %}">
  </a>
</div>

<!------------->
<!---- DIV ---->
<!------------->
<div class="text-box" markdown="1">
## What does the "awesome" in "awesome list" mean? 

It indicates a collection of interesting and helpful stuff — quality instead of quantity. 
The idea originates from [awesomelists.top](https://awesomelists.top/), which provides an excellent overview and a search function to explore legit awesome lists. 
It can include links to tutorials on programming languages, best practices on various topics, a list of great tools to improve productivity, and much more.

## Helpful List of useful Resources for Software Development

[The HIFIS Consulting Team][consulting-team] has curated an awesome list of important links, covering a variety of topics related to Research Software Development in the context of Open Science.
The list can be found on [HIFIS GitHub repo][github]. 
The list covers a broad range of topics, e.g. educational content on how to implement solid change management or how to improve your coding style. 
It also contains links to guidelines on how to distribute and use Open Source software or how to get in touch with the research software community.
</div>
</div>
<!--=======================-->
<!--=======================-->

**[The main repository][github]** serves as an entry point for all awesome lists generated by HIFIS. The following main topics we cover:

- [Educational Resources][educational-resources]
- [Communities][communities]
- [Find-ability & FAIR][find-ability-fair]
- [Policies & Guidelines][policies--guidelines]
- [Scientific][science]

## You can help

Feel free to suggest any awesome links to helpful content in the affiliated topic: 
Just create an issue in the corresponding repo. 
We appreciate every input — let the awesome list become longer and even more awesome!

Special thanks to [Ronny Gey](https://github.com/geyslein) who initially started the RSE awesome list.

<!-- Links -->
[github]: https://github.com/hifis-net/awesome-rse
[educational-resources]: https://github.com/hifis-net/awesome-rse-education
[communities]: https://github.com/hifis-net/awesome-rse-communities
[find-ability-fair]: https://github.com/hifis-net/awesome-rse-fair
[policies--guidelines]: https://github.com/hifis-net/awesome-rse-policies
[science]: https://github.com/hifis-net/awesome-rse-science
[consulting-team]: https://www.hifis.net/services/software/consulting.html

