---
title: "Out now: The 2nd issue of our newsletter"
title_image: paula-hayes-Eeee5H-yuoc-unsplash.jpg
data: 2022-10-25
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - Newsletter
excerpt: >
  Subscribe to our newsletter to get further issues!
---

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="two people talking about figures on flipchart"
src="{% link assets/img/hifisfor/poster-3.svg %}"
/>
</div>
<div class="text-box" markdown="1"> 
## We published the second issue of our quarterly newsletter! 
 
This autumn has been very busy here at HIFIS, as the HIFIS platform was evaluated with great results:
In the end, HIFIS was certified for its excellent development in all areas and a clear recommendation was made for the further operation and expansion of the platform. 
We reported on that in a [blog post](https://hifis.net/news/2022/10/06/hifis-evaluation) three weeks ago and included it of course in the issue.

Further topics we featured in our latest newsletter: 
- **Sam the Scientist** gets help from HIFIS Consulting and tells us about [the experience](https://www.hifis.net/newsletter/2022-10/sam_october.html).
- Two new **use cases**, [DAPHNE4NFDI](https://hifis.net/use-case/2022/09/14/DAPHNE4NFDI) and the [AI Research in Health Topics at CASUS](https://hifis.net/use-case/2022/08/10/use-case-health).
- **HIFIS Education** calls attention to the [TEACH 2 Conference](https://events.hifis.net/event/312/) on 9 November, 2022 and the [upcoming courses](https://events.hifis.net/category/4/).
- [A short reminder](https://hifis.net/news/2022/08/09/new-codebase-domain.html) from **Software Technology** that the URL of the Helmholtz Codebase will switch on 11 November, 2022.
- An announcement from **Helmholtz Cloud**: the first two **scientific services**, [Phileas Storage (Aircraft measurement campaign with HALO)](https://halo-research.de/) and [webODV (ocean data view by AWI)](https://hifis.net/use-case/2022/08/17/Ocean-Data-Viewer.html) will be made avaiable in the Cloud soon
</div>
</div>
<div class="clear"></div>

## Don't want to miss the next newsletter?
**Subscribe** [here](https://lists.desy.de/sympa/subscribe/hifis-newsletter) to get all further newsletters automatically!
The growing **archive** will be kept [here]({% link newsletter/index.md %}), 
where you find both the [HTML]({% link newsletter/2022-10/2022-10-HIFIS-Newsletter.md %}) and [PDF]({% link newsletter/2022-10/2022-10-HIFIS-Newsletter.pdf %}) version of the October issue.
