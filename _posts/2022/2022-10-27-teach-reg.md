---
title: "TEACH 2 conference: Registration is open"
title_image: photo-of-open-signage-2763246.jpg
date: 2022-10-27
authors:
  - "Stephanie Schworm"
  - klaffki
layout: blogpost
categories:
  - News
tags:
  - Education
  - Announcement
  - Event
  - HIDA
  - HMC
excerpt: >
  The second installment of the TEACH event is going to take place 9 November, 2022, in an online format. The registration is open now.
---

{:.float-left}
{:.treat-as-figure}
![Banner image for the TEACH II]({% link assets/img/posts/2022-10-27-teach2/HiDA_Visual_TEACH2_Visuals_16-9.jpeg %})

## Online Event for Education Topics across disciplines
Personal and professional development of all Helmholtz researchers — ranging from undergraduate students to established scientists — 
is a central part of the Helmholtz mission. 
All across the Association, efforts are underway to enable and ensure this.
This participant-driven event brings together Training Coordinators, Instructors and Trainers, Personnel Developers, and Researchers 
to exchange experience and best practices, collaborate and share resources on the whole education life cycle, and — of course — specific training programs.

Colleagues from different Helmholtz Centers and partnering institutions plan to give YOU the opportunity to network and dive into education topics across disciplines.

## Registration is open now

The online event takes place **9 November, 2022, 10.00 – 17.00**, and will be hosted in Gathertown. 
In the space you will be able to listen to talks, attend workshops, and of course network with each other! 
You will be able to learn more about our platforms, research schools, and more. 
Do not miss the opportunity get to know peers from other Helmholtz centers during the lunchbreak and in the after hour session!

You find the program [here](https://events.hifis.net/event/312/timetable/#20221109),
the registration is [open now](https://events.hifis.net/event/312/).

## Get in contact
HIFIS Support: support@hifis.net

HIDA: hida-courses@helmholtz.de

